Bomberguy
Copyright (C) 2015
Vicente Eduardo Ferrer Garc�a (vic798@gmail.com)
Alejandro Juan P�rez (tuketelamodelmon@gmail.com)


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


BRIEF

A Bomberman remake developed in C++ for NintendoDS using devkitPro.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


LICENSE

	GNU Affero General Public License 3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/> or
<http://www.affero.org/oagpl.html>.

	BY-NC-SA 3.0
	
This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

