/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include <nds.h>

#include "sprites.hpp"

int main()
{
	
	// test main BG
	setupGraphics();
	setBgBlock(1, 0, SOFT_BRICK_BLOCK_ID);
	setBgBlock(5, 5, SOFT_BRICK_BLOCK_ID);
	setBgBlock(3, 8, SOFT_BRICK_BLOCK_ID);
	setBgBlock(4, 11, SOFT_BRICK_BLOCK_ID);
	setBgBlock(4, 13, SOFT_BRICK_BLOCK_ID);
	setBgBlock(7, 8, SOFT_BRICK_BLOCK_ID);
	setBgBlock(3, 7, SOFT_BRICK_BLOCK_ID);
	
	// test fire BG
	setFireBgBlock(7, 7, FIRE_MIDDLE_BLOCK_ID);
	setFireBgBlock(7, 6, FIRE_VERT_BLOCK_ID);
	setFireBgBlock(7, 5, FIRE_UP_BLOCK_ID);
	setFireBgBlock(7, 8, FIRE_VERT_BLOCK_ID);
	setFireBgBlock(7, 9, FIRE_DOWN_BLOCK_ID);
	setFireBgBlock(6, 7, FIRE_HORI_BLOCK_ID);
	setFireBgBlock(5, 7, FIRE_LEFT_BLOCK_ID);
	setFireBgBlock(8, 7, FIRE_HORI_BLOCK_ID);
	setFireBgBlock(9, 7, FIRE_RIGHT_BLOCK_ID);
	
	// test sprites
	GuySprite guy1(0);
	guy1.hide(false);
	guy1.setPosition(32, 16);
	guy1.setAnimation(GuySprite::ANIMATION_WALKING_DOWN);
	
	GuySprite guy2(1);
	guy2.hide(false);
	guy2.setPosition(48, 16);
	guy2.setAnimation(GuySprite::ANIMATION_WALKING_UP);
	
	GuySprite guy3(2);
	guy3.hide(false);
	guy3.setPosition(64, 16);
	guy3.setAnimation(GuySprite::ANIMATION_WALKING_LEFT);
	
	GuySprite guy4(3);
	guy4.hide(false);
	guy4.setPosition(80, 16);
	guy4.setAnimation(GuySprite::ANIMATION_WALKING_RIGHT);
	
	GuySprite guy5(4);
	guy5.hide(false);
	guy5.setPosition(96, 16);
	guy5.setAnimation(GuySprite::ANIMATION_DIE);
	
	BombSprite bomb0(5);
	bomb0.hide(false);
	bomb0.setPosition(112, 16);
	//bomb0.setAnimation(BombSprite::ANIMATION_DEFAULT);
	
	BombSprite bomb1(6);
	bomb1.hide(false);
	bomb1.setPosition(128, 16);
	bomb1.setAnimation(BombSprite::ANIMATION_EXPLODE);
	
	EnemySprite enemy0(7);
	enemy0.hide(false);
	enemy0.setPosition(144, 16);
	enemy0.setAnimation(EnemySprite::ANIMATION_DEFAULT);
	
	EnemySprite enemy1(8);
	enemy1.hide(false);
	enemy1.setPosition(160, 16);
	enemy1.setAnimation(EnemySprite::ANIMATION_DIE);
	
	while(1)
	{
		
		guy1.update();
		guy2.update();
		guy3.update();
		guy4.update();
		guy5.update();
		
		bomb0.update();
		bomb1.update();
		
		enemy0.update();
		enemy1.update();
		
		swiWaitForVBlank();
		
	}
	
}
