#!/bin/bash

# run this script always at first
# this script creates a stub for gdb
desmume --arm9gdb=55554 --console-type=debug bomberguyd.nds

# if it shows output error as this one:
#	Slot 2: NONE
#	Failed to create ARM9 gdbstub on port 55554
#
# it's because of aborted and socket connection
# has been blocked
#
# use a new port configuring it from there and from
# gdb in C::B (Project->Properties->Debugger->
# Select Target: Debug -> IP address / Port)
