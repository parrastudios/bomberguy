/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "sprites.hpp"
#include "graphics_utils.hpp"

// BG
#include "grass.h"
#include "block.h"
#include "brick.h"
#include "red_brick.h"
#include "door.h"

#include "fire_middle.h"
#include "fire_down_hand.h"
#include "fire_up_hand.h"
#include "fire_left_hand.h"
#include "fire_right_hand.h"
#include "fire_vert_arm.h"
#include "fire_hori_arm.h"

// OBJ
#include "guy_down_0.h"
#include "guy_down_1.h"
#include "guy_down_2.h"
#include "guy_up_0.h"
#include "guy_up_1.h"
#include "guy_up_2.h"
#include "guy_left_0.h"
#include "guy_left_1.h"
#include "guy_left_2.h"
#include "guy_right_0.h"
#include "guy_right_1.h"
#include "guy_right_2.h"
#include "guy_die_0.h"
#include "guy_die_1.h"
#include "guy_die_2.h"
#include "guy_die_3.h"
#include "guy_die_4.h"

#include "bomb_0.h"
#include "bomb_1.h"
#include "bomb_2.h"

#include "enemy_0.h"
#include "enemy_1.h"
#include "enemy_2.h"
#include "enemy_3.h"

void setupGraphics()
{
	
	irqEnable( IRQ_VBLANK );
	
	// set mode 0 | enable BG 0 | enable BG 1 | enable sprites | 1D layout
	videoSetMode
	(
		MODE_0_2D |
		DISPLAY_BG0_ACTIVE | DISPLAY_BG1_ACTIVE |
		DISPLAY_SPR_ACTIVE |
		DISPLAY_SPR_1D_LAYOUT
	);
	
	vramSetBankE( VRAM_E_MAIN_BG );
    vramSetBankF( VRAM_F_MAIN_SPRITE );
    
    // BACKGROUND
    
    // copy BG graphics to VRAM
    dmaCopyHalfWords( 3, grassTiles, tile2bgram( 4*GRASS_BLOCK_ID ), grassTilesLen );
    dmaCopyHalfWords( 3, blockTiles, tile2bgram( 4*HARD_BRICK_BLOCK_ID ), blockTilesLen );
    dmaCopyHalfWords( 3, brickTiles, tile2bgram( 4*SOFT_BRICK_BLOCK_ID ), brickTilesLen );
    dmaCopyHalfWords( 3, red_brickTiles, tile2bgram( 4*RED_BRICK_BLOCK_ID ), red_brickTilesLen );
    dmaCopyHalfWords( 3, doorTiles, tile2bgram( 4*DOOR_BLOCK_ID ), doorTilesLen );
    dmaCopyHalfWords( 3, fire_middleTiles, tile2bgram( 4*FIRE_MIDDLE_BLOCK_ID ), fire_middleTilesLen );
    dmaCopyHalfWords( 3, fire_down_handTiles, tile2bgram( 4*FIRE_DOWN_BLOCK_ID ), fire_down_handTilesLen );
    dmaCopyHalfWords( 3, fire_up_handTiles, tile2bgram( 4*FIRE_UP_BLOCK_ID ), fire_up_handTilesLen );
    dmaCopyHalfWords( 3, fire_left_handTiles, tile2bgram( 4*FIRE_LEFT_BLOCK_ID ), fire_left_handTilesLen );
    dmaCopyHalfWords( 3, fire_right_handTiles, tile2bgram( 4*FIRE_RIGHT_BLOCK_ID ), fire_right_handTilesLen );
    dmaCopyHalfWords( 3, fire_vert_armTiles, tile2bgram( 4*FIRE_VERT_BLOCK_ID ), fire_vert_armTilesLen );
    dmaCopyHalfWords( 3, fire_hori_armTiles, tile2bgram( 4*FIRE_HORI_BLOCK_ID ), fire_hori_armTilesLen );
    // blank block for the fire bg
    for(int i=0; i<16; i++) tile2bgram( 4*COUNT_BLOCK_ID+0 )[i] = 0;
    for(int i=0; i<16; i++) tile2bgram( 4*COUNT_BLOCK_ID+1 )[i] = 0;
    for(int i=0; i<16; i++) tile2bgram( 4*COUNT_BLOCK_ID+2 )[i] = 0;
    for(int i=0; i<16; i++) tile2bgram( 4*COUNT_BLOCK_ID+3 )[i] = 0;
    
    // copy BG pal to VRAM
    dmaCopyHalfWords( 3, grassPal, pal2bgram(0), grassPalLen );
    dmaCopyHalfWords( 3, blockPal, pal2bgram(1), blockPalLen );
    dmaCopyHalfWords( 3, fire_middlePal, pal2bgram(2), fire_middlePalLen );
    // block and brick share the same palette
    
    // configure the BG settings registers
    // with this line we indicate that the bg tilemap is
    // stored in the second (1) base4 block of the VRAM
    // each base block occupies 2KB
    // we are using the second because the first is used
    // by the bg tiles (grass)
    REG_BG0CNT = BG_MAP_BASE(7) | 1;	// priority 1
    REG_BG1CNT = BG_MAP_BASE(8);		// priority 0
    
    // fill the tile map (all the tiles are the same)
    
    for(int i=0; i<256; i++)
    {
		
		const int x = i%16;
		const int y = i/16;
		int pal = bgPals[i];
		int block = bgBlocks[i];
		
		// top left
		bg_map[ 2*x+2*y*32 ] =
		pal << 12 |							// select palette
		(block*4);		// select tile
		
		// top right
		bg_map[ 2*x+2*y*32+1 ] =
		pal << 12 |							// select palette
		(block*4+1);	// select tile
		
		// bot left
		bg_map[ 2*x+2*y*32+32 ] =
		pal << 12 |							// select palette
		(block*4+2);	// select tile
		
		// bot right
		bg_map[ 2*x+2*y*32+32+1 ] =
		pal << 12 |							// select palette
		(block*4+3);	// select tile
		
	}
	
	// fill the tilemap of the fire bg
	for(int i=0; i<1024; i++)
    {
		
		const int firePal = blockTypePal[FIRE_MIDDLE_BLOCK_ID];
		fire_bg_map[i] = 4*COUNT_BLOCK_ID | firePal << 12;
		
	}
	
    
    
    // SPRITES
    
    // disable all sprites
    const int spritesCount = 128;
    for(int i=0; i<spritesCount; i++)
    {
		sprites[i].attr0 = ATTR0_DISABLED;
	}
    
    // load guy tile data
    // down 0
    dmaCopyHalfWords
    (
		3,
		guy_down_0Tiles,
		tile2objram( 4*SPRITE_GUY_DOWN_0 ),
		guy_down_0TilesLen
	);
    
    // down 1
    dmaCopyHalfWords
    (
		3,
		guy_down_1Tiles,
		tile2objram( 4*SPRITE_GUY_DOWN_1 ),
		guy_down_1TilesLen
	);
    
    // down 2
    dmaCopyHalfWords
    (
		3,
		guy_down_2Tiles,
		tile2objram( 4*SPRITE_GUY_DOWN_2 ),
		guy_down_2TilesLen
	);
	
	// up 0
    dmaCopyHalfWords
    (
		3,
		guy_up_0Tiles,
		tile2objram( 4*SPRITE_GUY_UP_0 ),
		guy_up_0TilesLen
	);
	
	// up 1
    dmaCopyHalfWords
    (
		3,
		guy_up_1Tiles,
		tile2objram( 4*SPRITE_GUY_UP_1 ),
		guy_up_1TilesLen
	);
	
	// up 2
    dmaCopyHalfWords
    (
		3,
		guy_up_2Tiles,
		tile2objram( 4*SPRITE_GUY_UP_2 ),
		guy_up_2TilesLen
	);
	
	// left 0
    dmaCopyHalfWords
    (
		3,
		guy_left_0Tiles,
		tile2objram( 4*SPRITE_GUY_LEFT_0 ),
		guy_left_0TilesLen
	);
    
    // left 1
    dmaCopyHalfWords
    (
		3,
		guy_left_1Tiles,
		tile2objram( 4*SPRITE_GUY_LEFT_1 ),
		guy_left_1TilesLen
	);
	
	// left 2
    dmaCopyHalfWords
    (
		3,
		guy_left_2Tiles,
		tile2objram( 4*SPRITE_GUY_LEFT_2 ),
		guy_left_2TilesLen
	);
	
	// right 0
    dmaCopyHalfWords
    (
		3,
		guy_right_0Tiles,
		tile2objram( 4*SPRITE_GUY_RIGHT_0 ),
		guy_right_0TilesLen
	);
	
	// right 1
    dmaCopyHalfWords
    (
		3,
		guy_right_1Tiles,
		tile2objram( 4*SPRITE_GUY_RIGHT_1 ),
		guy_right_1TilesLen
	);
	
	// right 2
    dmaCopyHalfWords
    (
		3,
		guy_right_2Tiles,
		tile2objram( 4*SPRITE_GUY_RIGHT_2 ),
		guy_right_2TilesLen
	);
	
	// die 0
    dmaCopyHalfWords
    (
		3,
		guy_die_0Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_0 ),
		guy_die_0TilesLen
	);
	
	// die 1
    dmaCopyHalfWords
    (
		3,
		guy_die_1Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_1 ),
		guy_die_1TilesLen
	);
	
	// die 2
    dmaCopyHalfWords
    (
		3,
		guy_die_2Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_2 ),
		guy_die_2TilesLen
	);
	
	// die 3
    dmaCopyHalfWords
    (
		3,
		guy_die_3Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_3 ),
		guy_die_3TilesLen
	);
	
	// die 4
    dmaCopyHalfWords
    (
		3,
		guy_die_4Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_4 ),
		guy_die_4TilesLen
	);
	
	// bomb 0
	dmaCopyHalfWords
    (
		3,
		bomb_0Tiles,
		tile2objram( 4*SPRITE_BOMB_0 ),
		bomb_0TilesLen
	);
	
	// bomb 1
	dmaCopyHalfWords
    (
		3,
		bomb_1Tiles,
		tile2objram( 4*SPRITE_BOMB_1 ),
		bomb_1TilesLen
	);
	
	// bomb 2
	dmaCopyHalfWords
    (
		3,
		bomb_2Tiles,
		tile2objram( 4*SPRITE_BOMB_2 ),
		bomb_2TilesLen
	);
    
    // enemy 0
    dmaCopyHalfWords
    (
		3,
		enemy_0Tiles,
		tile2objram( 4*SPRITE_ENEMY_0 ),
		enemy_0TilesLen
	);
	
	// enemy 1
    dmaCopyHalfWords
    (
		3,
		enemy_1Tiles,
		tile2objram( 4*SPRITE_ENEMY_1 ),
		enemy_1TilesLen
	);
	
	// enemy 2
    dmaCopyHalfWords
    (
		3,
		enemy_2Tiles,
		tile2objram( 4*SPRITE_ENEMY_2 ),
		enemy_2TilesLen
	);
	
	// enemy 3
    dmaCopyHalfWords
    (
		3,
		enemy_3Tiles,
		tile2objram( 4*SPRITE_ENEMY_3 ),
		enemy_3TilesLen
	);
    
    // load guy palette (all the frames of the guy share the same palette)
    // also the bomb is using the same palette
    // also the enemy is using the same palette
    dmaCopyHalfWords
    (
		3,
		guy_down_0Pal,
		pal2objram( PALETTE_GUY ),
		guy_down_0PalLen
    );
	
}

void setBgBlock(int x, int y, BlockType blockType)
{
	
	const int pal = blockTypePal[blockType];
	const int block = blockType;
	
	// top left
	bg_map[ 2*x+2*y*32 ] =
	pal << 12 |							// select palette
	(block*4);		// select tile
	
	// top right
	bg_map[ 2*x+2*y*32+1 ] =
	pal << 12 |							// select palette
	(block*4+1);	// select tile
	
	// bot left
	bg_map[ 2*x+2*y*32+32 ] =
	pal << 12 |							// select palette
	(block*4+2);	// select tile
	
	// bot right
	bg_map[ 2*x+2*y*32+32+1 ] =
	pal << 12 |							// select palette
	(block*4+3);	// select tile
	
}

void setFireBgBlock(int x, int y, BlockType blockType)
{
	
	const int pal = blockTypePal[blockType];
	int block = blockType;
	
	// top left
	fire_bg_map[ 2*x+2*y*32 ] =
	pal << 12 |							// select palette
	(block*4);		// select tile
	
	// top right
	fire_bg_map[ 2*x+2*y*32+1 ] =
	pal << 12 |							// select palette
	(block*4+1);	// select tile
	
	// bot left
	fire_bg_map[ 2*x+2*y*32+32 ] =
	pal << 12 |							// select palette
	(block*4+2);	// select tile
	
	// bot right
	fire_bg_map[ 2*x+2*y*32+32+1 ] =
	pal << 12 |							// select palette
	(block*4+3);	// select tile
	
}

Sprite::Sprite(int slot)
{
	
	this->slot = slot;
	spriteEntry = &sprites[slot];
	spriteEntry->attr1&= ~(0x3<<14);
	spriteEntry->attr1 |= (0x1<<14);		// set size flag to 16x16
		
}

void Sprite::setSprite(int sprite)
{
	
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= sprite;
	
}

void Sprite::setPalette(int palette)
{
	
	spriteEntry->attr2 &= ~(0xF<<12);
	spriteEntry->attr2 |= (palette<<12);
	
}

void Sprite::setPriority(int priority)
{
	
	spriteEntry->attr2 &= ~(0x3<<10);
	spriteEntry->attr2 |= (priority<<10);
	
}

void Sprite::setPosition(s16 x, s16 y)
{
	
	y &= 0xFF;
	spriteEntry->attr0 &= ~(0xFF);		// set last byte to 0
	spriteEntry->attr0 |= y;
	
	x &= 0x1FF;
	spriteEntry->attr1 &= ~(0x1FF);
	spriteEntry->attr1 |= x;
	
}

void Sprite::hide(bool b)
{
	
	// hide
	if(b)
	{
		spriteEntry->attr0 &= ~(1<<8);		// set bit 8 to 0
		spriteEntry->attr0 |= (1<<9);		// set bit 9 to 1
	}
	
	// show
	else
	{
		spriteEntry->attr0 &= ~(1<<8);		// set bit 8 to 0
		spriteEntry->attr0 &= ~(1<<9);		// set bit 9 to 0
	}
	
}

int Sprite::getSlot()const
{
	return slot;
}


// GUY

GuySprite::GuySprite(int slot)
:
Sprite(slot)
{
	
	animationStep = 0;
	animation = ANIMATION_STAND_DOWN;
	setSprite(SPRITE_GUY_DOWN_0);
	
}

void GuySprite::setSprite(int sprite)
{
	
	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;
	
}

void GuySprite::setAnimation(int animation)
{
	
	this->animation = animation;
	frameCount = 0;
	animationStep = 0;
	
	if( animation == ANIMATION_STAND_DOWN )
	{
		setSprite(SPRITE_GUY_DOWN_0);
	}
	
	else if( animation == ANIMATION_STAND_UP )
	{
		setSprite(SPRITE_GUY_UP_0);
	}
	
	else if( animation == ANIMATION_STAND_LEFT )
	{
		setSprite(SPRITE_GUY_LEFT_0);
	}
	
	else if( animation == ANIMATION_STAND_RIGHT )
	{
		setSprite(SPRITE_GUY_RIGHT_0);
	}
	
	
	else if( animation == ANIMATION_WALKING_DOWN )
	{
		setSprite(SPRITE_GUY_DOWN_0);
	}
	
	else if( animation == ANIMATION_WALKING_UP )
	{
		setSprite(SPRITE_GUY_UP_0);
	}
	
	else if( animation == ANIMATION_WALKING_LEFT )
	{
		setSprite(SPRITE_GUY_LEFT_0);
	}
	
	else if( animation == ANIMATION_WALKING_RIGHT )
	{
		setSprite(SPRITE_GUY_RIGHT_0);
	}
	
	else if( animation == ANIMATION_DIE )
	{
		setSprite(SPRITE_GUY_DIE_0);
	}
	
}

int GuySprite::getAnimation()const
{
	return animation;
}


void GuySprite::update()
{
	
	if( animation == ANIMATION_STAND_DOWN )
	{
		// nothing to do here
	}
	
	else if( animation == ANIMATION_STAND_UP )
	{
		// nothing to do here
	}
	
	else if( animation == ANIMATION_STAND_LEFT )
	{
		// nothing to do here
	}
	
	else if( animation == ANIMATION_STAND_RIGHT )
	{
		// nothing to do here
	}
	
	// animation walking down
	else if( animation == ANIMATION_WALKING_DOWN )
	{
		
		if(frameCount >= 12)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_DOWN_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_DOWN_0;
			}
			else if( animationStep == 3 )
			{
				sprite = SPRITE_GUY_DOWN_2;
			}
			else if( animationStep == 4 )
			{
				sprite = SPRITE_GUY_DOWN_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
	// animation walking up
	else if( animation == ANIMATION_WALKING_UP )
	{
		
		if(frameCount >= 12)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_UP_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_UP_0;
			}
			else if( animationStep == 3 )
			{
				sprite = SPRITE_GUY_UP_2;
			}
			else if( animationStep == 4 )
			{
				sprite = SPRITE_GUY_UP_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
	// animation walking left
	else if( animation == ANIMATION_WALKING_LEFT )
	{
		
		if(frameCount >= 20)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_LEFT_2;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_LEFT_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
	// animation walking right
	else if( animation == ANIMATION_WALKING_RIGHT )
	{
		
		if(frameCount >= 20)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_RIGHT_2;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_RIGHT_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
	
	// animation die
	else if( animation == ANIMATION_DIE )
	{
		
		if(frameCount >= 20)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_DIE_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_DIE_2;
			}
			else if( animationStep == 3 )
			{
				sprite = SPRITE_GUY_DIE_3;
			}
			else if( animationStep == 4 )
			{
				sprite = SPRITE_GUY_DIE_4;
			}
			else if( animationStep == 5 )
			{
				sprite = SPRITE_GUY_DIE_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
}


// BOMB

BombSprite::BombSprite(int slot)
:
Sprite(slot)
{
	
	animationStep = 0;
	animation = ANIMATION_DEFAULT;
	setSprite(SPRITE_BOMB_0);
	
}

void BombSprite::setSprite(int sprite)
{
	
	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;
	
}

void BombSprite::setAnimation(int animation)
{
	
	this->animation = animation;
	
	if( animation == ANIMATION_DEFAULT )
	{
		setSprite(SPRITE_BOMB_0);
	}
	
	else if( animation == ANIMATION_EXPLODE )
	{
		setSprite(SPRITE_BOMB_2);
	}
	
}

int BombSprite::getAnimation()const
{
	return animation;
}

void BombSprite::update()
{
	
	// default animation of the bomb
	if( animation == ANIMATION_DEFAULT )
	{
		
		if(frameCount >= 30)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_BOMB_2;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_BOMB_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
	// explode animation
	if( animation == ANIMATION_EXPLODE )
	{
		
		if(frameCount >= 30)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_BOMB_0;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_BOMB_1;
				animationStep = 1;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
}


// ENEMY
EnemySprite::EnemySprite(int slot)
:
Sprite(slot)
{
	
	animationStep = 0;
	animation = ANIMATION_DEFAULT;
	setSprite(SPRITE_ENEMY_0);
	
}

void EnemySprite::setSprite(int sprite)
{
	
	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;
	
}

void EnemySprite::setAnimation(int animation)
{
	
	frameCount = 0;
	animationStep = 0;
	this->animation = animation;
	
	if( animation == ANIMATION_DEFAULT )
	{
		setSprite(SPRITE_ENEMY_0);
	}
	
	else if( animation == ANIMATION_DIE )
	{
		setSprite(SPRITE_ENEMY_2);
	}
	
}

int EnemySprite::getAnimation()const
{
	return animation;
}

void EnemySprite::update()
{
	
	// default animation of the enemy
	if( animation == ANIMATION_DEFAULT )
	{
		
		if(frameCount >= 30)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_ENEMY_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_ENEMY_0;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
	// die animation
	if( animation == ANIMATION_DIE )
	{
		
		if(frameCount >= 50)	// change frame
		{
			
			animationStep++;
			sprite = animationStep;
			
			if( animationStep == 1 )
			{
				sprite = SPRITE_ENEMY_3;
				animationStep = 0;
			}
			
			setSprite(sprite);
			frameCount = 0;
			
		}
		
		frameCount++;
		
	}
	
}


SpriteFactory::SpriteFactory()
:freeSlots(128)
{
	
	for(int i=0; i<128; i++)
	{
		freeSlots[i] = 127-i;	// [127 .. 0]
	}
	
}

GuySprite* SpriteFactory::createGuySprite()
{
	
	int slot = freeSlots.back();
	freeSlots.pop_back();
	
	GuySprite* guySprite = new GuySprite(slot);
	guySprite->hide(false);
	
	return guySprite;
	
}

EnemySprite* SpriteFactory::createEnemySprite()
{
	
	int slot = freeSlots.back();
	freeSlots.pop_back();
	
	EnemySprite* enemySprite = new EnemySprite(slot);
	enemySprite->hide(false);
	
	return enemySprite;
	
}

BombSprite* SpriteFactory::createBombSprite()
{
	
	int slot = freeSlots.back();
	freeSlots.pop_back();
	
	BombSprite* bombSprite = new BombSprite(slot);
	bombSprite->hide(false);
	
	return bombSprite;
	
}

void SpriteFactory::destroyGuySprite( GuySprite* guySprite )
{
	
	int slot = guySprite->getSlot();
	freeSlots.push_back(slot);
	guySprite->hide(true);
	delete guySprite;
	
}

void SpriteFactory::destroyEnemySprite( EnemySprite* enemySprite )
{
	
	int slot = enemySprite->getSlot();
	freeSlots.push_back(slot);
	enemySprite->hide(true);
	delete enemySprite;
	
}

void SpriteFactory::destroyBombSprite( BombSprite* bombSprite )
{
	
	int slot = bombSprite->getSlot();
	freeSlots.push_back(slot);
	bombSprite->hide(true);
	delete bombSprite;
	
}

