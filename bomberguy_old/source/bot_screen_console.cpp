/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "bot_screen_console.hpp"
#include <stdio.h>

using namespace std;

void setupBotScreen()
{
	
	consoleDemoInit();
	
	iprintf("\n          BOMBERGUY\n\n");
	iprintf("\n\n          lives:");
	iprintf("\n\n          points:");
	
}

void displayLives( u16 lives )
{
	
	iprintf( "\x1b[5;16H           " );
	iprintf( "\x1b[5;16H%d", lives );
	
}

void displayPoints( u16 points )
{
	
	iprintf( "\x1b[7;17H           " );
	iprintf( "\x1b[7;17H%d", points );
	
}

void displayCustomMsg( const string msg )
{
	
	cleanCustomMsg();
	
	iprintf( "\x1b[12;8H%s", msg.c_str() );
	
}

void cleanCustomMsg()
{
	
	iprintf( "\x1b[12;14H                           " );
	
}
