/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////


#ifndef BOX_HPP
#define BOX_HPP

class Box
{
	
	int x, y;			// 20.12
	int width, height;	// 20.12

public:

	Box();
	Box(int x, int y, int width, int height);
	
	// getters
	int getX()const;
	int getY()const;
	int getWidth()const;
	int getHeight()const;
	
	// setters
	void setX(int x);
	void setY(int y);
	void setPosition(int x, int y);
	void setWidth(int width);
	void setHeight(int height);
	
	bool checkCollision(const Box& box);
	
};

#endif
