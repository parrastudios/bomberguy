/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include <nds.h>
#include <maxmod9.h>

#ifndef SOUND_HPP
#define SOUND_HPP

enum SoundTrack
{
	SOUND_POKEMON,
	SOUND_COLLISION,
	SOUND_DIE,
	SOUND_EXPLOSION
};

extern mm_sound_effect collideSound;
extern mm_sound_effect explosionSound;

void setupSound();

void setMainTheme(SoundTrack soundTrack);

void playSound(SoundTrack soundTrack);


#endif
