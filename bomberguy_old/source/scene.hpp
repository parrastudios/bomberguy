/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "sprites.hpp"
#include "entity.hpp"
#include <vector>

#ifndef SCENE_HPP
#define SCENE_HPP

class Scene : public Singleton<Scene>
{
	
	// definitions
	class BlockGrid
	{
		
		BlockType blocks[12][16];
		
	public:
		
		BlockType getBlockType(int x, int y)const;
		void setBlockType(int x, int y, BlockType blockType);
		
	};
	
	struct Explosion
	{
		u16 countTime;
		s16 centerX, centerY;
		s16 downLimit, upLimit, leftLimit, rightLimit;
	};
	
	// attributes
	BlockGrid blockGrid;
	
	static const s16 SPAWN_BLOCK_X = 0;
	static const s16 SPAWN_BLOCK_Y = 1;
	GuyEntity guy;
	
	static const int MAX_NUM_BOMBS = 3;
	std::vector<BombEntity*> bombs;
	
	static const int MAX_NUM_ENEMIES = 16;
	std::vector<EnemyEntity*> enemies;
	
	std::vector<Explosion> explosions;
	
	s16 timeToStart;	// when game over or change of level,
						// this is the time to start a new level
	
public:
	
	Scene();
	
	void update();
	
	GuyEntity* getGuy();
	void respawn();
	
	BombEntity* createBomb(s16 blockX, s16 blockY);		// block format
	void destroyBomb(BombEntity* bombEntity);
	bool checkIfThereIsBomb(s16 x, s16 y);		// block format
	
	EnemyEntity* createEnemy(s16 blockX, s16 blockY);		// block format
	void destroyEnemy(EnemyEntity* enemyEntity);
	int getEnemiesCount()const;
	
	void createExplosion(s16 centerX, s16 centerY);
	
	int getBlock(int x, int y)const;
	void setBlock(int x, int y, BlockType blockType);
	
	void gameover();
	void win();
	
	void generateNewRandomLevel();
	
private:
	
	void drawFire
	(
		s16 centerX, s16 centerY,
		s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
	);
	
	void cleanFire
	(
		s16 centerX, s16 centerY,
		s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
	);
	
};

#endif
