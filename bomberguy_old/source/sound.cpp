/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "sound.hpp"

#include <nds.h>
#include <maxmod9.h>
#include "soundbank.h"
#include "soundbank_bin.h"

void setupSound()
{
	
	soundEnable();
	mmInitDefaultMem((mm_addr)soundbank_bin);
	mmLoad( MOD_POKEMON_GYM );
	mmLoadEffect( SFX_COLLIDE );
	mmLoadEffect( SFX_EXPLOSION );
	
}


void setMainTheme(SoundTrack soundTrack)
{
	
	if( soundTrack == SOUND_POKEMON )
	{
		
		mmStart( MOD_POKEMON_GYM, MM_PLAY_LOOP );
		mmSetModuleVolume(256);
		
	}
	
}

void playSound(SoundTrack soundTrack)
{
	
	if( soundTrack == SOUND_COLLISION )
	{
		
		mmEffectEx(&collideSound);
		
	}
	else if( soundTrack == SOUND_EXPLOSION )
	{
		
		mmEffectEx(&explosionSound);
		
	}
	
}


// SOUND EFFECTS

mm_sound_effect collideSound =
{
	{ SFX_COLLIDE } ,			// id
	(int)(1.0f * (1<<10)),	// rate
	0,		// handle
	255,	// volume
	127,		// panning
};

mm_sound_effect explosionSound =
{
	{ SFX_EXPLOSION } ,			// id
	(int)(1.0f * (1<<10)),	// rate
	0,		// handle
	255,	// volume
	127,		// panning
};
