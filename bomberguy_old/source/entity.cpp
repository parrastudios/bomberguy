/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "entity.hpp"
#include "sprites.hpp"
#include "scene.hpp"
#include <vector>
#include "randomizer.hpp"
#include "bot_screen_console.hpp"

using namespace std;


Entity::Entity(Scene* myScene)
{
	this->myScene = myScene;
}

int Entity::getX()const
{
	return box.getX();
}

int Entity::getY()const
{
	return box.getY();
}

Box* Entity::getBox()
{
	return &box;
}

// GUY

GuyEntity::GuyEntity(Scene* myScene)
:
Entity(myScene)
{
	
	SpriteFactory& spriteFactory = SpriteFactory::get_instance();
	guySprite = spriteFactory.createGuySprite();
	speedX = (3<<9);
	speedY = (3<<9);
	targetX = 0;
	targetY = 0;
	lives = START_LIVES;
	points = 0;
	inmuneTime = 100;
	timeToDie = -1;
	
	displayLives(lives);
	displayPoints(points);
	
}

GuySprite* GuyEntity::getSprite()
{
	return guySprite;
}

bool GuyEntity::isOnTarget()
{
	
	return getX()==getTargetX() && getY()==getTargetY();
	
}

void GuyEntity::setPosition(int x, int y)
{
	
	box.setPosition(x, y);
	
	// 20.12 -> int
	guySprite->setPosition( x>>12, y>>12 );
	
}

int GuyEntity::getTargetX()const
{
	return targetX;
}

int GuyEntity::getTargetY()const
{
	return targetY;
}

void GuyEntity::setTarget(int x, int y)
{
	targetX = x;
	targetY = y;
}

void GuyEntity::update()
{
	
	if( inmuneTime > 0 ) inmuneTime--;
	
	
	if( guySprite->getAnimation() != GuySprite::ANIMATION_DIE )
	{
		if( targetX != getX() )
		{
			
			int prev_sign = (targetX < getX()) ? -1 : 1;
			
			int npos = getX() + prev_sign * speedX;
			
			int curr_sign = (targetX < npos) ? -1 : 1;
			
			if( prev_sign != curr_sign )
			{
				npos = targetX;
			}
			
			// the guy arrived to the target -> stop animating
			if( npos == targetX )
			{
				if( prev_sign == -1 )
				{
					guySprite->setAnimation(GuySprite::ANIMATION_STAND_LEFT);
				}
				else
				{
					guySprite->setAnimation(GuySprite::ANIMATION_STAND_RIGHT);
				}
			}
			
			setPosition( npos, getY() );
			
		}
		else if( targetY != getY() )
		{
			
			int prev_sign = (targetY < getY()) ? -1 : 1;
			
			int npos = getY() + prev_sign * speedY;
			
			int curr_sign = (targetY < npos) ? -1 : 1;
			
			if( prev_sign != curr_sign )
			{
				
				npos = targetY;
				
			}
			
			setPosition( getX(), npos );
			
			// the guy arrived to the target -> stop animating
			if( npos == targetY )
			{
				if( prev_sign == -1 )
				{
					guySprite->setAnimation(GuySprite::ANIMATION_STAND_UP);
				}
				else
				{
					guySprite->setAnimation(GuySprite::ANIMATION_STAND_DOWN);
				}
			}
			
		}
		
	}
	
	// dying
	else
	{
		
		if( timeToDie == 0 )
		{
			
			timeToDie = -1;
			
			// gameover
			if( lives == 0 )
			{
				
				myScene->gameover();
				
			}
			
			else
			{
				
				lives--;
				displayLives( lives );
				
				// respawn
				myScene->respawn();
				
				inmuneTime = 120;
				
			}
			
		}
		else
		{
			timeToDie--;
		}
		
		
	}
	
	guySprite->update();
	
}

void GuyEntity::kill()
{
	
	if
	(
		inmuneTime == 0 &&
		guySprite->getAnimation() != GuySprite::ANIMATION_DIE
	)
	{
		
		guySprite->setAnimation( GuySprite::ANIMATION_DIE );
		timeToDie = 60;
		
	}
	
}

u16 GuyEntity::getLives()const
{
	return lives;
}

void GuyEntity::setLives(u16 lives)
{
	this->lives = lives;
	displayLives(lives);
}

void GuyEntity::setLives()
{
	this->lives = START_LIVES;
	displayLives(lives);
}

u16 GuyEntity::getPoints()const
{
	return points;
}

void GuyEntity::setPoints(u16 points)
{
	this->points = points;
	displayPoints(points);
}

GuyEntity::~GuyEntity()
{
	SpriteFactory& spriteFactory = SpriteFactory::get_instance();
	spriteFactory.destroyGuySprite(guySprite);
}


// BOMB

BombEntity::BombEntity(Scene* myScene)
:
Entity(myScene)
{
	
	SpriteFactory& spriteFactory = SpriteFactory::get_instance();
	bombSprite = spriteFactory.createBombSprite();
	countTime = 0;
	
}

BombSprite* BombEntity::getSprite()
{
	return bombSprite;
}

void BombEntity::setPosition(int blockX, int blockY)	// block format
{
	
	this->blockX = blockX;
	this->blockY = blockY;
	bombSprite->setPosition(16*blockX, 16*blockY);
	
}		

int BombEntity::getBlockX()const
{
	return blockX;
}

int BombEntity::getBlockY()const
{
	return blockY;
}

void BombEntity::update()
{
	
	bombSprite->update();
	
	if( bombSprite->getAnimation() == BombSprite::ANIMATION_DEFAULT )
	{
		
		countTime++;
		
		if( countTime > 200 )
		{
			
			bombSprite->setAnimation( BombSprite::ANIMATION_EXPLODE );
			countTime = 0;
			
		}
		
	}
	
	else if( bombSprite->getAnimation() == BombSprite::ANIMATION_EXPLODE )
	{
		
		countTime++;
		
		if( countTime > 100 )
		{
			
			myScene->createExplosion(blockX, blockY);
			myScene->destroyBomb( this );
			
			
		}
		
	}
	
	
	
}

BombEntity::~BombEntity()
{
	
	SpriteFactory& spriteFactory = SpriteFactory::get_instance();
	spriteFactory.destroyBombSprite(bombSprite);
	
}


// ENEMY

EnemyEntity::EnemyEntity(Scene* myScene)
:
Entity(myScene)
{
	
	SpriteFactory& spriteFactory = SpriteFactory::get_instance();
	enemySprite = spriteFactory.createEnemySprite();
	speedX = 1<<10;
	speedY = 1<<10;
	targetX = 0;
	targetY = 0;
	
}

EnemySprite* EnemyEntity::getSprite()
{
	return enemySprite;
}

void EnemyEntity::setPosition(int x, int y)		// 20.12 format
{
	
	box.setPosition(x, y);
	
	// 20.12 -> int
	enemySprite->setPosition( x>>12, y>>12 );
	
}

int EnemyEntity::getTargetX()const
{
	return targetX;
}

int EnemyEntity::getTargetY()const
{
	return targetY;
}

void EnemyEntity::setTarget(int x, int y)
{
	targetX = x;
	targetY = y;
}

void EnemyEntity::setNewRandomTarget()
{
	
	const int blockX = (getX()>>12)/16;
	const int blockY = (getY()>>12)/16;
	
	enum
	{
		DOWN,
		UP,
		LEFT,
		RIGHT
	};
	
	vector<int> options;
	
	if
	(
		myScene->getBlock( blockX, blockY+1 ) == GRASS_BLOCK_ID &&
		!myScene->checkIfThereIsBomb( blockX, blockY+1 )
	)
	{
		options.push_back( DOWN );
	}
	
	if
	(
		myScene->getBlock( blockX, blockY-1 ) == GRASS_BLOCK_ID &&
		!myScene->checkIfThereIsBomb( blockX, blockY-1 )
	)
	{
		options.push_back( UP );
	}
	
	if
	(
		myScene->getBlock( blockX-1, blockY ) == GRASS_BLOCK_ID &&
		!myScene->checkIfThereIsBomb( blockX-1, blockY )
	)
	{
		options.push_back( LEFT );
	}
	
	if
	(
		myScene->getBlock( blockX+1, blockY ) == GRASS_BLOCK_ID &&
		!myScene->checkIfThereIsBomb( blockX+1, blockY )
	)
	{
		options.push_back( RIGHT );
	}
	
	const int numOptions = options.size();
	
	if( numOptions == 0 ) return;
	
	const int rndInt = Randomizer::random(0, numOptions-1);
	
	int direction = options[ rndInt ];
	
	if( direction == DOWN )
	{
		setTarget( (blockX*16)<<12, (((blockY+1)*16))<<12  );
	}
	else if( direction == UP )
	{
		setTarget( (blockX*16)<<12, ((blockY-1)*16)<<12  );
	}
	else if( direction == LEFT )
	{
		setTarget( ((blockX-1)*16)<<12, (blockY*16)<<12  );
	}
	else if( direction == RIGHT )
	{
		setTarget( ((blockX+1)*16)<<12, (blockY*16)<<12  );
	}
	
}

bool EnemyEntity::isOnTarget()
{
	
	return getX()==getTargetX() && getY()==getTargetY();
	
}

void EnemyEntity::update()
{
	
	if( enemySprite->getAnimation() == EnemySprite::ANIMATION_DEFAULT )
	{
		
		if( targetX != getX() )
		{
			
			int prev_sign = (targetX < getX()) ? -1 : 1;
			
			int npos = getX() + prev_sign * speedX;
			
			int curr_sign = (targetX < npos) ? -1 : 1;
			
			if( prev_sign != curr_sign )
			{
				npos = targetX;
			}
			
			setPosition( npos, getY() );
			
			// the enemy arrived to the target -> choose a new target
			if( npos == targetX )
			{
				
				setNewRandomTarget();
				
			}
			
		}
		else if( targetY != getY() )
		{
			
			int prev_sign = (targetY < getY()) ? -1 : 1;
			
			int npos = getY() + prev_sign * speedY;
			
			int curr_sign = (targetY < npos) ? -1 : 1;
			
			if( prev_sign != curr_sign )
			{
				
				npos = targetY;
				
			}
			
			setPosition( getX(), npos );
			
			// the enemy arrived to the target -> choose a new target
			if( npos == targetY )
			{
				
				setNewRandomTarget();
				
			}
			
		}
		
		enemySprite->update();
		
	}
	
	else if( enemySprite->getAnimation() == EnemySprite::ANIMATION_DIE )
	{
		
		if( timeToDie <= 0)
		{
			myScene->destroyEnemy( this );
		}
		else
		{
			timeToDie--;
			enemySprite->update();
		}
		
	}
	
	
}

void EnemyEntity::kill()
{
	
	enemySprite->setAnimation( EnemySprite::ANIMATION_DIE );
	timeToDie = 50;
	
}

EnemyEntity::~EnemyEntity()
{
	
	SpriteFactory& spriteFactory = SpriteFactory::get_instance();
	spriteFactory.destroyEnemySprite(enemySprite);
	
}

