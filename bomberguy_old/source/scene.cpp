/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "scene.hpp"
#include "bot_screen_console.hpp"
#include "stdio.h"
#include "randomizer.hpp"
#include "sound.hpp"

Scene::Scene()
:
guy(this)
{
	
	int x, y;
	x = 0;
	y = (1*16)<<12;
	guy.setPosition(x, y);
	guy.setTarget(x, y);
	
	for(int y=0; y<12; y++)
	for(int x=0; x<16; x++)
	{
		
		if( x%2 || y%2 )
		{
			blockGrid.setBlockType(x, y, GRASS_BLOCK_ID);
		}
		else
		{
			blockGrid.setBlockType(x, y, HARD_BRICK_BLOCK_ID);
		}
		
	}
	
}

void Scene::update()
{
	
	guy.update();
	
	if( timeToStart > 0 )
	{
		timeToStart--;
	}
	else if( timeToStart == 0 )
	{
		timeToStart = -1;
		generateNewRandomLevel();
	}
	
	for(u16 i=0; i<bombs.size(); i++)
	{
		bombs[i]->update();
	}
	
	for(u16 i=0; i<enemies.size(); i++)
	{
		enemies[i]->update();
	}
	
	
	// check collisions
	
	Box* guyBox = guy.getBox();
		
	for(u16 i=0; i<enemies.size(); i++)
	{
		
		Box* enemyBox = enemies[i]->getBox();
		
		if( enemyBox->checkCollision( *guyBox ) )
		{
			guy.kill();
		}
		
	}
	
	// manage explosions
	for(u16 i=0; i<explosions.size(); i++)
	{
		
		Explosion& explosion = explosions[i];
		
		// end explosion
		if( explosion.countTime >= 30 )
		{
			
			// red bricks disappear
			if( getBlock(explosion.centerX, explosion.downLimit+1) == RED_BRICK_BLOCK_ID )
			{
				setBlock(explosion.centerX, explosion.downLimit+1, GRASS_BLOCK_ID );
			}
			if( getBlock(explosion.centerX, explosion.upLimit-1) == RED_BRICK_BLOCK_ID )
			{
				setBlock(explosion.centerX, explosion.upLimit-1, GRASS_BLOCK_ID );
			}
			if( getBlock(explosion.leftLimit-1, explosion.centerY) == RED_BRICK_BLOCK_ID )
			{
				setBlock(explosion.leftLimit-1, explosion.centerY, GRASS_BLOCK_ID );
			}
			if( getBlock(explosion.rightLimit+1, explosion.centerY) == RED_BRICK_BLOCK_ID )
			{
				setBlock(explosion.rightLimit+1, explosion.centerY, GRASS_BLOCK_ID );
			}
			
			// clean fire
			cleanFire
			(
				explosion.centerX,
				explosion.centerY,
				explosion.downLimit,
				explosion.upLimit,
				explosion.leftLimit,
				explosion.rightLimit
			);
			
			explosions[i] = explosions.back();
			explosions.pop_back();
			i--;
			
		}
		
		// continue explosion
		else
		{
			
			drawFire
			(
				explosion.centerX,
				explosion.centerY,
				explosion.downLimit,
				explosion.upLimit,
				explosion.leftLimit,
				explosion.rightLimit
			);
			
			explosion.countTime++;
			
		}
		
	}
	
}

GuyEntity* Scene::getGuy()
{
	return &guy;
}

void Scene::respawn()
{
	
	guy.setPosition( (SPAWN_BLOCK_X*16) <<12, (SPAWN_BLOCK_Y*16) <<12 );
	guy.setTarget( (SPAWN_BLOCK_X*16) <<12, (SPAWN_BLOCK_Y*16) <<12 );
	guy.getSprite()->setAnimation( GuySprite::ANIMATION_STAND_DOWN );
	
}

BombEntity* Scene::createBomb(s16 x, s16 y)
{
	
	if( bombs.size() < MAX_NUM_BOMBS )
	{
		
		
		bool isOccupied = false;
		for(u16 i=0; i<bombs.size(); i++)
		{
			if( bombs[i]->getBlockX() == x && bombs[i]->getBlockY() == y )
			{
				isOccupied = true;
			}
		}
		
		if( !isOccupied )
		{
			
			BombEntity* bombEntity = new BombEntity( this );
			
			bombs.push_back( bombEntity );
			bombEntity->setPosition( x, y );
			
			return bombEntity;
			
		}
		
	}
	
	// too many bombs
	else
	{
		// playSound( SOUND_ERROR );
		
		return 0;
	}
	
	return 0;
	
}

void Scene::destroyBomb(BombEntity* bombEntity)
{
	
	int bombIndex = 0;
	
	while( bombs[bombIndex] != bombEntity ) bombIndex++;
	
	bombs[ bombIndex ] = bombs.back();
	bombs.pop_back();
	
	delete bombEntity;
	
}

bool Scene::checkIfThereIsBomb(s16 x, s16 y)		// block format
{
	
	
	bool isOccupied = false;
	for(u16 i=0; i<bombs.size(); i++)
	{
		if( bombs[i]->getBlockX() == x && bombs[i]->getBlockY() == y )
		{
			isOccupied = true;
		}
	}
	
	return isOccupied;
	
}

EnemyEntity* Scene::createEnemy(s16 blockX, s16 blockY)		// block format
{
	
	if( enemies.size() < MAX_NUM_ENEMIES )
	{
		
		EnemyEntity* enemyEntity = new EnemyEntity( this );
		
		enemies.push_back( enemyEntity );
		enemyEntity->setPosition( (blockX*16)<<12, (blockY*16)<<12 );
		enemyEntity->setNewRandomTarget();
		
		return enemyEntity;
		
	}
	
	return 0;
	
}

void Scene::destroyEnemy(EnemyEntity* enemyEntity)
{
	
	int enemyIndex = 0;
	
	while( enemies[enemyIndex] != enemyEntity ) enemyIndex++;
	
	enemies[ enemyIndex ] = enemies.back();
	enemies.pop_back();
	
	delete enemyEntity;
	
}

int Scene::getEnemiesCount()const
{
	return enemies.size();
}

void Scene::createExplosion(s16 centerX, s16 centerY)
{
	
	// boooom
	playSound( SOUND_EXPLOSION );
	
	// Compute the soft blocks to destroy
	
	int downLimit = centerY, upLimit = centerY;
	int leftLimit = centerX, rightLimit = centerX;
	
	while( getBlock(centerX, downLimit) == GRASS_BLOCK_ID ) downLimit++;
	
	if( getBlock(centerX, downLimit) == SOFT_BRICK_BLOCK_ID )
	{
		blockGrid.setBlockType(centerX, downLimit, RED_BRICK_BLOCK_ID );
	}
	
	while( getBlock(centerX, upLimit) == GRASS_BLOCK_ID ) upLimit--;
	
	if( getBlock(centerX, upLimit) == SOFT_BRICK_BLOCK_ID )
	{
		blockGrid.setBlockType(centerX, upLimit, RED_BRICK_BLOCK_ID );
	}
	
	while( getBlock(leftLimit, centerY) == GRASS_BLOCK_ID ) leftLimit--;
	
	if( getBlock(leftLimit, centerY) == SOFT_BRICK_BLOCK_ID )
	{
		blockGrid.setBlockType(leftLimit, centerY, RED_BRICK_BLOCK_ID );
	}
	
	while( getBlock(rightLimit, centerY) == GRASS_BLOCK_ID ) rightLimit++;
	if( getBlock(rightLimit, centerY) == SOFT_BRICK_BLOCK_ID )
	{
		blockGrid.setBlockType(rightLimit, centerY, RED_BRICK_BLOCK_ID );
	}
	
	
	downLimit--;
	upLimit++;
	leftLimit++;
	rightLimit--;
	
	// kill the enemies
	
	Box horiBox
	(
		(leftLimit*16) <<12,
		(centerY*16) <<12,
		((rightLimit-leftLimit+1)*16) <<12,
		16 <<12
	);
	
	Box vertBox
	(
		(centerX*16) <<12,
		(upLimit*16) <<12,
		16 <<12,
		((downLimit-upLimit+1)*16) <<12
	);
	
	// kill enemies
	for(u16 i=0; i<enemies.size(); i++)
	{
		
		if
		(
			horiBox.checkCollision( *(enemies[i]->getBox()) ) ||
			vertBox.checkCollision( *(enemies[i]->getBox()) )
		)
		{
			enemies[i]->kill();
		}
		
	}
	
	// kill guy
	if
	(
		horiBox.checkCollision( (*guy.getBox()) ) ||
		vertBox.checkCollision( (*guy.getBox()) )
	)
	{
		guy.kill();
	}
	
	Explosion explosion;
	explosion.countTime = 0;
	explosion.centerX = centerX;
	explosion.centerY = centerY;
	explosion.downLimit = downLimit;
	explosion.upLimit = upLimit;
	explosion.leftLimit = leftLimit;
	explosion.rightLimit = rightLimit;
	
	explosions.push_back( explosion );
	
}

int Scene::getBlock(int x, int y)const
{
	return blockGrid.getBlockType(x, y);
}

void Scene::setBlock(int x, int y, BlockType blockType)
{
	
	blockGrid.setBlockType( x, y, blockType );
	
}


BlockType Scene::BlockGrid::getBlockType(int x, int y)const
{
	
	// is out of bounds
	if
	(
		x < 0 ||
		y < 0 ||
		x >= 16 ||
		y >= 12
	)
	{
		return HARD_BRICK_BLOCK_ID;
	}
	
	return blocks[y][x];
	
}

void Scene::BlockGrid::setBlockType(int x, int y, BlockType blockType)
{
	
	blocks[y][x] = blockType;
	setBgBlock(x, y, blockType);
	
}

void Scene::gameover()
{
	
	displayCustomMsg( "GAME OVER" );
	timeToStart = 60;
	guy.setPoints(0);
	guy.setLives();
	
}

void Scene::win()
{
	displayCustomMsg( "YOU WIN!!!" );
	timeToStart = 60;
	guy.setPoints( guy.getPoints()+1 );
}

void Scene::generateNewRandomLevel()
{
	
	// clean soft blocks
	for(int y=0; y<12; y++)
	for(int x=0; x<16; x++)
	{
		
		if( x%2==1 || y%2==1 )
		{
			setBlock(x, y, GRASS_BLOCK_ID);
		}
		
	}
	
	for(int y=3; y<12; y++)
	for(int x=3; x<16; x++)
	{
		
		if( x%2==1 || y%2==1 )
		{
			int rnd = Randomizer::random(0, 3);
			if( rnd == 0 )
			{
				setBlock(x, y, SOFT_BRICK_BLOCK_ID);
			}
		}
		
	}
	
	for(int y=3; y<12; y++)
	for(int x=3; x<16; x++)
	{
		
		if( x%2==1 || y%2==1 )
		{
			int rnd = Randomizer::random(0, 8);
			if( rnd == 0 && getBlock(x, y) == GRASS_BLOCK_ID )
			{
				createEnemy(x, y);
			}
		}
		
	}
	
	setBlock(15, 11, DOOR_BLOCK_ID);
	
	respawn();
	
}

void Scene::drawFire
(
	s16 centerX, s16 centerY,
	s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
)
{
	
	setFireBgBlock( centerX, centerY, FIRE_MIDDLE_BLOCK_ID );
	
	if( downLimit != centerY )
	{
		
		setFireBgBlock( centerX, downLimit, FIRE_DOWN_BLOCK_ID );
		
		downLimit--;
		while( downLimit != centerY)
		{
			setFireBgBlock( centerX, downLimit, FIRE_VERT_BLOCK_ID);
			downLimit--;
		}
		
	}
	
	if ( upLimit != centerY )
	{
		
		setFireBgBlock( centerX, upLimit, FIRE_UP_BLOCK_ID );
		
		upLimit++;
		while( upLimit != centerY)
		{
			setFireBgBlock( centerX, upLimit, FIRE_VERT_BLOCK_ID);
			upLimit++;
		}
		
	}
	
	if ( leftLimit != centerX )
	{
		
		setFireBgBlock( leftLimit, centerY, FIRE_LEFT_BLOCK_ID );
		
		leftLimit++;
		while( leftLimit != centerX)
		{
			setFireBgBlock( leftLimit, centerY, FIRE_HORI_BLOCK_ID);
			leftLimit++;
		}
		
	}
	
	if ( rightLimit != centerX )
	{
		
		setFireBgBlock( rightLimit, centerY, FIRE_RIGHT_BLOCK_ID );
		
		rightLimit--;
		while( rightLimit != centerX)
		{
			setFireBgBlock( rightLimit, centerY, FIRE_HORI_BLOCK_ID);
			rightLimit--;
		}
		
	}

	
}

void Scene::cleanFire
(
	s16 centerX, s16 centerY,
	s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
)
{
	
	setFireBgBlock( centerX, centerY, FIRE_BLANK_BLOCK_ID );
	
	if( downLimit != centerY )
	{
		
		setFireBgBlock( centerX, downLimit, FIRE_BLANK_BLOCK_ID );
		
		downLimit--;
		while( downLimit != centerY)
		{
			setFireBgBlock( centerX, downLimit, FIRE_BLANK_BLOCK_ID );
			downLimit--;
		}
		
	}
	
	if ( upLimit != centerY )
	{
		
		setFireBgBlock( centerX, upLimit, FIRE_BLANK_BLOCK_ID );
		
		upLimit++;
		while( upLimit != centerY)
		{
			setFireBgBlock( centerX, upLimit, FIRE_BLANK_BLOCK_ID );
			upLimit++;
		}
		
	}
	
	if ( leftLimit != centerX )
	{
		
		setFireBgBlock( leftLimit, centerY, FIRE_BLANK_BLOCK_ID );
		
		leftLimit++;
		while( leftLimit != centerX)
		{
			setFireBgBlock( leftLimit, centerY, FIRE_BLANK_BLOCK_ID );
			leftLimit++;
		}
		
	}
	
	if ( rightLimit != centerX )
	{
		
		setFireBgBlock( rightLimit, centerY, FIRE_BLANK_BLOCK_ID );
		
		rightLimit--;
		while( rightLimit != centerX)
		{
			setFireBgBlock( rightLimit, centerY, FIRE_BLANK_BLOCK_ID );
			rightLimit--;
		}
		
	}
	
}
