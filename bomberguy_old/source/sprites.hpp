/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include <nds.h>
#include "graphics_utils.hpp"
#include "singleton.hpp"
#include <vector>


#ifndef SPRITES_HPP
#define SPRITES_HPP


// BACKGROUND

// tile map of the bg
const int bgBlocks[] =
{
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const int bgPals[] =
{
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

enum BlockType
{
	GRASS_BLOCK_ID = 0,
	HARD_BRICK_BLOCK_ID,
	SOFT_BRICK_BLOCK_ID,
	RED_BRICK_BLOCK_ID,
	DOOR_BLOCK_ID,
	
	FIRE_MIDDLE_BLOCK_ID,
	FIRE_DOWN_BLOCK_ID,
	FIRE_UP_BLOCK_ID,
	FIRE_LEFT_BLOCK_ID,
	FIRE_RIGHT_BLOCK_ID,
	FIRE_VERT_BLOCK_ID,
	FIRE_HORI_BLOCK_ID,
	FIRE_BLANK_BLOCK_ID,
	
	COUNT_BLOCK_ID
	
};

const int blockTypePal[] =
{
	0,	// GRASS_BLOCK_ID
	1,	// HARD_BRICK_BLOCK_ID
	1,	// SOFT_BRICK_BLOCK_ID
	1,	// RED_BRICK_BLOCK_ID
	1,	// DOOR_BLOCK_ID
	
	2,	// FIRE_MIDDLE_BLOCK_ID
	2,	// FIRE_DOWN_BLOCK_ID
	2,	// FIRE_UP_BLOCK_ID
	2,	// FIRE_LEFT_BLOCK_ID
	2,	// FIRE_RIGHT_BLOCK_ID
	2,	// FIRE_VERT_BLOCK_ID
	2,	// FIRE_HORI_BLOCK_ID
	2,	// FIRE_BLANK_BLOCK_ID
};

/* *
 * This function allows to indiviadually set the blocks
 * of the background(16x16)
 * */
void setBgBlock(int x, int y, BlockType blockType);
void setFireBgBlock(int x, int y, BlockType blockType);

// SETUP

void setupGraphics();



// SPRITES

enum
{
	
	SPRITE_GUY_DOWN_0 = 0,
	SPRITE_GUY_DOWN_1,
	SPRITE_GUY_DOWN_2,
	SPRITE_GUY_UP_0,
	SPRITE_GUY_UP_1,
	SPRITE_GUY_UP_2,
	SPRITE_GUY_LEFT_0,
	SPRITE_GUY_LEFT_1,
	SPRITE_GUY_LEFT_2,
	SPRITE_GUY_RIGHT_0,
	SPRITE_GUY_RIGHT_1,
	SPRITE_GUY_RIGHT_2,
	SPRITE_GUY_DIE_0,
	SPRITE_GUY_DIE_1,
	SPRITE_GUY_DIE_2,
	SPRITE_GUY_DIE_3,
	SPRITE_GUY_DIE_4,
	
	SPRITE_BOMB_0,
	SPRITE_BOMB_1,
	SPRITE_BOMB_2,
	
	SPRITE_ENEMY_0,
	SPRITE_ENEMY_1,
	SPRITE_ENEMY_2,
	SPRITE_ENEMY_3,
	
};

// palettes
enum
{
	PALETTE_GUY = 0
};

class Sprite
{
	
	int slot;
	
protected:
	
	Sprite(int slot);
	SpriteAttrEntry * spriteEntry;
	
	virtual void setSprite(int sprite);
	void setPalette(int palette);
	void setPriority(int priority);
	
public:
	
	void setPosition(s16 x, s16 y);
	virtual void setAnimation(int animation) = 0;
	virtual int getAnimation()const = 0;
	
	void hide(bool b);
	
	// call this function in each frame
	virtual void update(){};
	
	int getSlot()const;
	
	virtual ~Sprite(){}
	
};

class GuySprite : public Sprite
{
	
	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation
	
	void setSprite(int sprite);
	
public:
	
	GuySprite(int slot);

	enum
	{
		ANIMATION_STAND_DOWN,
		ANIMATION_STAND_UP,
		ANIMATION_STAND_LEFT,
		ANIMATION_STAND_RIGHT,
		ANIMATION_WALKING_DOWN,
		ANIMATION_WALKING_UP,
		ANIMATION_WALKING_LEFT,
		ANIMATION_WALKING_RIGHT,
		ANIMATION_DIE
	};
	
	void setAnimation(int animation);
	int getAnimation()const;
	
	void update();
	
	~GuySprite(){}
	
};

class BombSprite : public Sprite
{
	
	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation
	
	void setSprite(int sprite);
	
public:
	
	BombSprite(int slot);
	
	enum
	{
		ANIMATION_DEFAULT,
		ANIMATION_EXPLODE
	};
	
	void setAnimation(int animation);
	int getAnimation()const;
	
	void update();
	
	~BombSprite(){}
	
};

class EnemySprite : public Sprite
{
	
	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation
	
	void setSprite(int sprite);
	
public:
	
	EnemySprite(int slot);
	
	enum
	{
		ANIMATION_DEFAULT,
		ANIMATION_DIE
	};
	
	void setAnimation(int animation);
	int getAnimation()const;
	
	void update();
	
	~EnemySprite(){}
	
};

/**
 * Use this singleton each time you want to
 * create a new sprite
 **/

class SpriteFactory : public Singleton<SpriteFactory>
{

private:
	
	std::vector<int> freeSlots;
	
public:

	SpriteFactory();
	
	GuySprite* createGuySprite();
	EnemySprite* createEnemySprite();
	BombSprite* createBombSprite();
	
	void destroyGuySprite( GuySprite* guySprite );
	void destroyEnemySprite( EnemySprite* enemySprite );
	void destroyBombSprite( BombSprite* bombSprite );
	
};

#endif
