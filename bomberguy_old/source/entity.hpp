/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SCENE_HPP
class Scene;
#endif

#include "box.hpp"
#include "sprites.hpp"

#ifndef ENTITY_HPP
#define ENTITY_HPP

static const int WIDTH_TILES = 16;
static const int HEIGHT_TILES = 12;

class Entity
{
	
protected:

	Scene* myScene;
	Box box;
	
public:
	
	Entity(Scene* myScene);
	
	virtual Sprite* getSprite() = 0;
	
	// format dependent on the type of entity
	virtual void setPosition(int x, int y) = 0;
	virtual int getX()const;
	virtual int getY()const;
	
	Box* getBox();
	
	virtual void update() = 0;
	
	virtual ~Entity(){}
	
};

class GuyEntity : public Entity
{
	
	GuySprite* guySprite;
	int targetX, targetY;
	
	unsigned speedX, speedY;
	
	u16 lives;
	u16 points;
	u16 inmuneTime;
	s16 timeToDie;		// time to end die animation
	
	static u16 const START_LIVES = 5;
	
public:
	
	GuyEntity(Scene* myScene);
	
	GuySprite* getSprite();
	void setPosition(int x, int y);		// 20.12 format
	
	int getTargetX()const;
	int getTargetY()const;
	void setTarget(int x, int y);
	
	bool isOnTarget();
	
	void update();
	
	void kill();
	
	u16 getLives()const;
	void setLives(u16 lives);
	void setLives();
	
	u16 getPoints()const;
	void setPoints(u16 points);
	
	~GuyEntity();
	
};

class BombEntity : public Entity
{
	
	BombSprite* bombSprite;
	int countTime;
	int blockX, blockY;
	
public:
	
	BombEntity(Scene* myScene);
	
	BombSprite* getSprite();
	void setPosition(int blockX, int blockY);		// block format
	
	int getBlockX()const;
	int getBlockY()const;
	
	void update();
	
	~BombEntity();
	
};

class EnemyEntity : public Entity
{
	
	EnemySprite* enemySprite;
	int targetX, targetY;
	
	unsigned speedX, speedY;
	
	s16 timeToDie;
	
public:
	
	EnemyEntity(Scene* myScene);
	
	EnemySprite* getSprite();
	void setPosition(int x, int y);		// 20.12 format
	
	int getTargetX()const;
	int getTargetY()const;
	void setTarget(int x, int y);
	
	u16 getLives()const;
	void setLives(u16 lives);
	
	void setNewRandomTarget();
	
	bool isOnTarget();
	
	void update();
	
	void kill();
	
	~EnemyEntity();
	
};

#endif
