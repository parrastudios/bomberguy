/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "box.hpp"

Box::Box()
{
	x = 0;
	y = 0;
	width = 16<<12;
	height = 16<<12;
}

Box::Box(int x, int y, int width, int height)
{
	
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	
}

int Box::getX()const
{
	return x;
}

int Box::getY()const
{
	return y;
}

int Box::getWidth()const
{
	return width;
}

int Box::getHeight()const
{
	return height;
}

void Box::setX(int x)
{
	this->x = x;
}

void Box::setY(int y)
{
	this->y = y;
}

void Box::setPosition(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Box::setWidth(int width)
{
	this->width = width;
}

void Box::setHeight(int height)
{
	this->height = height;
}

bool Box::checkCollision(const Box& box)
{
	
	// these incs/decs are a trick to set a margin
	int x1 = x+1;
	int y1 = y+1;
	int x2 = box.x+1;
	int y2 = box.y+1;
	int width1 = width-1;
	int height1 = height-1;
	int width2 = box.width-1;
	int height2 = box.height-1;
	
	
	bool res =
	(
	
		(
			(x1 <= x2 && x2 <= x1+width1) ||
			(x1 <= x2+width2 && x2+width2 <= x1+width1)
		)
		&&
		(
			(y1 <= y2 && y2 <= y1+height1) ||
			(y1 <= y2+height2 && y2+height2 <= y1+height1)
		)
	)
	||
	(
		(
			(x2 <= x1 && x1 <= x2+width2) ||
			(x2 <= x1+width1 && x1+width1 <= x2+width2)
		)
		&&
		(
			(y2 <= y1 && y1 <= y2+height2) ||
			(y2 <= y1+height1 && y1+height1 <= y2+height2)
		)
	);
	
	return res;
	
}
