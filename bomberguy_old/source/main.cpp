/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include <nds.h>
#include <stdio.h>

#include "scene.hpp"
#include "sprites.hpp"
#include "sound.hpp"
#include "randomizer.hpp"
#include "bot_screen_console.hpp"

void processInput();

int main()
{
	
	Randomizer::initialize();
	
	// test main BG
	setupGraphics();
	setupSound();
	setupBotScreen();
	
	setMainTheme(SOUND_POKEMON);
	
	Scene& scene = Scene::get_instance();
	GuyEntity* guyEntity = scene.getGuy();
	
	/*
	EnemyEntity* enemyEntity = scene.createEnemy(5, 5);
	
	scene.setBlock(3, 3, SOFT_BRICK_BLOCK_ID);
	scene.setBlock(4, 1, SOFT_BRICK_BLOCK_ID);
	scene.setBlock(5, 9, SOFT_BRICK_BLOCK_ID);
	scene.setBlock(13, 4, SOFT_BRICK_BLOCK_ID);
	scene.setBlock(9, 8, SOFT_BRICK_BLOCK_ID);
	scene.setBlock(7, 9, SOFT_BRICK_BLOCK_ID);
	scene.setBlock(7, 10, SOFT_BRICK_BLOCK_ID);
	
	scene.setBlock(15, 11, DOOR_BLOCK_ID);
	*/
	
	scene.generateNewRandomLevel();
	
	while(1)
	{
		
		processInput();
		scene.update();
		
		swiWaitForVBlank();
		
	}
	
}

void processInput()
{
	
	scanKeys();
	
	Scene& scene = Scene::get_instance();
	GuyEntity* guy = scene.getGuy();
	
	int keys = keysHeld();
	
	// only respond to input if the guy is not moving
	// otherwise ignore
	if
	(
		guy->isOnTarget() &&
		guy->getSprite()->getAnimation() != GuySprite::ANIMATION_DIE
	)
	{
		
		int cellX = (guy->getX()>>12)/16;
		int cellY = (guy->getY()>>12)/16;
		
		if( keys & KEY_DOWN )
		{
			
			int blockType = scene.getBlock(cellX, cellY+1);
			int nEnemies = scene.getEnemiesCount();
			
			if
			(
				blockType == DOOR_BLOCK_ID &&
				nEnemies == 0
			)
			{
				
				scene.win();
				
			}
			
			bool occupied = 
				blockType == HARD_BRICK_BLOCK_ID ||
				blockType == SOFT_BRICK_BLOCK_ID ||
				blockType == RED_BRICK_BLOCK_ID ||
				scene.checkIfThereIsBomb(cellX, cellY+1);
			
			if(occupied)
			{
				guy->getSprite()->setAnimation(GuySprite::ANIMATION_STAND_DOWN);
				playSound(SOUND_COLLISION);
			}
			else
			{
				guy->setTarget( guy->getX(), ((cellY+1)<<12)*16 );
				guy->getSprite()->setAnimation( GuySprite::ANIMATION_WALKING_DOWN );
			}
			
		}
		
		else if( keys & KEY_UP )
		{
			
			int blockType = scene.getBlock(cellX, cellY-1);
			int nEnemies = scene.getEnemiesCount();
			
			if
			(
				blockType == DOOR_BLOCK_ID &&
				nEnemies == 0
			)
			{
				
				scene.win();
				
			}
			
			bool occupied = 
				blockType == HARD_BRICK_BLOCK_ID ||
				blockType == SOFT_BRICK_BLOCK_ID ||
				blockType == RED_BRICK_BLOCK_ID ||
				scene.checkIfThereIsBomb(cellX, cellY-1);
			
			if(occupied)
			{
				guy->getSprite()->setAnimation(GuySprite::ANIMATION_STAND_UP);
				playSound(SOUND_COLLISION);
			}
			else
			{
				guy->setTarget( guy->getX(), ((cellY-1)<<12)*16 );
				guy->getSprite()->setAnimation( GuySprite::ANIMATION_WALKING_UP );
			}
			
		}
		
		else if( keys & KEY_LEFT )
		{
			
			int blockType = scene.getBlock(cellX-1, cellY);
			int nEnemies = scene.getEnemiesCount();
			
			if
			(
				blockType == DOOR_BLOCK_ID &&
				nEnemies == 0
			)
			{
				
				scene.win();
				
			}
			
			bool occupied = 
				blockType == HARD_BRICK_BLOCK_ID ||
				blockType == SOFT_BRICK_BLOCK_ID ||
				blockType == RED_BRICK_BLOCK_ID ||
				scene.checkIfThereIsBomb(cellX-1, cellY);
			
			if(occupied)
			{
				guy->getSprite()->setAnimation(GuySprite::ANIMATION_STAND_LEFT);
				playSound(SOUND_COLLISION);
			}
			else
			{
				guy->setTarget( ((cellX-1)<<12)*16, guy->getY() );
				guy->getSprite()->setAnimation( GuySprite::ANIMATION_WALKING_LEFT );
			}
			
		}
		
		else if( keys & KEY_RIGHT )
		{
			
			int blockType = scene.getBlock(cellX+1, cellY);
			int nEnemies = scene.getEnemiesCount();
			
			if
			(
				blockType == DOOR_BLOCK_ID &&
				nEnemies == 0
			)
			{
				
				scene.win();
				
			}
			
			bool occupied = 
				blockType == HARD_BRICK_BLOCK_ID ||
				blockType == SOFT_BRICK_BLOCK_ID ||
				blockType == RED_BRICK_BLOCK_ID ||
				scene.checkIfThereIsBomb(cellX+1, cellY);
			
			if(occupied)
			{
				guy->getSprite()->setAnimation(GuySprite::ANIMATION_STAND_RIGHT);
				playSound(SOUND_COLLISION);
			}
			else
			{
				guy->setTarget( ((cellX+1)<<12)*16, guy->getY() );
				guy->getSprite()->setAnimation( GuySprite::ANIMATION_WALKING_RIGHT );
			}
			
		}
		
		else if( keys & KEY_A )
		{
			
			int x = guy->getX();
			int y = guy->getY();
			x = (x >> 12) / 16;		// 20.12 -> block
			y = (y >> 12) / 16;
			
			scene.createBomb(x, y);
			
		}
		
	}
	
}
