/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GRAPHICS_UTILS_HPP
#define GRAPHICS_UTILS_HPP



// tile index -> BG tile VRAM addr
#define tile2bgram(t) (BG_GFX + (t) * 16)

// pal index -> BG palette VRAM addr
#define pal2bgram(t) (BG_PALETTE + (t) * 16)

// tile index -> sprite tile VRAM addr
#define tile2objram(t) (SPRITE_GFX + (t) * 16)

// pal index -> sprite palette VRAM addr
#define pal2objram(t) (SPRITE_PALETTE + (t) * 16)

#define backdrop_color RGB8( 190, 225, 255 )

// addresses of the tile maps
#define bg_map    ((u16*)BG_MAP_RAM(7))
#define fire_bg_map ((u16*)BG_MAP_RAM(8))

// settings entry of sprites
typedef struct t_spriteEntry
{
    u16 attr0;
    u16 attr1;
    u16 attr2;
    u16 affine_data;
} SpriteAttrEntry;

#define sprites ((SpriteAttrEntry*)OAM)


#endif
