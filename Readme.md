![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2015/Feb/17/2172812680-1-bomberguy-logo_avatar.png "Bomberguy")

# __Bomberguy__
##### Copyright (C) 2015
##### Vicente Eduardo Ferrer García (<vic798@gmail.com>)
##### Alejandro Juan Pérez (<tuketelamodelmon@gmail.com>)


- - -


##### BRIEF

A Bomberman remake developed in C++ for NintendoDS using devkitPro.


- - -


##### LICENSE


![](http://www.gnu.org/graphics/agplv3-155x51.png "GNU Affero General Public License 3.0")

Copyright (C) 2009 - 2014

Vicente Eduardo Ferrer García - Parra Studios (<vic798@gmail.com>)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>


![](http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png "Creative Commons License")

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/ "License")


- - -

