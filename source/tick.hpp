/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef TICK_HPP
#define TICK_HPP

#include <vector>
#include <list>

#include "singleton.hpp"

class TickManager : public Singleton<TickManager> {

  public:
	typedef void (*on_tick_callback)(void * object);

	void initialize(int frequency);

	void clear();

	void tick();

	void register_callback(void * object, on_tick_callback callback, int tick_freq, bool loop);

  private:
	class TickCallback {

	  public:
		TickCallback(void * obj, on_tick_callback c, int f, bool l);

		~TickCallback();

		bool tick();

        bool is_loop();

	  private:
        void * object;
		on_tick_callback callback;
		int freq;
		int count;
		bool loop;
	};

	std::vector<TickCallback> callback_list;
	std::list<TickCallback> to_insert_list;

	bool inserting;
};

#endif // TICK_HPP
