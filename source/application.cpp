/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef LIBNDS_HEADER_INCLUDED
#define LIBNDS_HEADER_INCLUDED
#   include <nds.h>
#endif // LIBNDS_HEADER_INCLUDED

#include "application.hpp"
#include "randomizer.hpp"
#include "sprites.hpp"
#include "sound.hpp"
#include "tick.hpp"
#include "entity_manager.hpp"

#ifdef DEBUG
#   define TICK_MANAGER_UPDATE_PERIOD 100
#else
#   define TICK_MANAGER_UPDATE_PERIOD 25
#endif // DEBUG

void private_logic_callback(void * object) {
	// update game objects (move things around, calculate velocities, etc)
	Application::get_instance().update_logic();
}

void private_render_callback(void * object) {
	Application::get_instance().update_graphics();
}

void Application::update_logic() {
	EntityManager::get_instance().update();
}

void Application::update_graphics() {
	EntityManager::get_instance().render();
}

void Application::initialize() {

	// set irqs
	irqInit();
	irqEnable(IRQ_VBLANK);

	// set tick manager (100 ticks per second)
    TickManager::get_instance().initialize(TICK_MANAGER_UPDATE_PERIOD);

	// Register update callbacks
	TickManager::get_instance().register_callback(NULL, &private_logic_callback, 1, true);
	// ...

	// set randomizer
	Randomizer::get_instance().initialize();

	// set graphics
	Sprites::get_instance().setup_graphics();

    // set sound
    SoundManager::get_instance().setup_sound();

    // set main sound
    SoundManager::get_instance().set_main_theme(SoundManager::THEME_POKEMON);

	// load scene
	// scene.load(Scene::SCENE_TEST_ID);
	scene.load(Scene::SCENE_RANDOM_ID);

	// set mode 0 | enable the BG 0 | enable the BG 1 | enable sprites | 1D layout
	videoSetMode(MODE_0_2D | DISPLAY_BG0_ACTIVE | DISPLAY_BG1_ACTIVE | DISPLAY_SPR_ACTIVE | DISPLAY_SPR_1D_LAYOUT);
}

void Application::run() {

	// main loop
	while (1) {

		// update game objects (move things around, calculate velocities, etc)
		// update_logic();

		// wait for the vblank period
		swiWaitForVBlank();

		// vblank period (safe to modify graphics)
		update_graphics();
	}
}
