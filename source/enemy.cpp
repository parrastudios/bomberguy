/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "enemy.hpp"
#include "guy.hpp"
#include "scene.hpp"
#include "sprites.hpp"

void private_on_enemy_death(void * object) {
    Enemy * enemy = (Enemy *)object;

    enemy->set_valid(false);

    // give a powerup ?
}

Enemy::Enemy(Scene * s) : Entity(ENEMY_TYPE_ID_KEY, s) {
	set_sprite(SpriteFactory::get_instance().createEnemySprite());
	set_collidable(true);
}

Enemy::Enemy(Scene * s, int initial_x, int initial_y) : Entity(ENEMY_TYPE_ID_KEY, s, initial_x, initial_y) {
	set_sprite(SpriteFactory::get_instance().createEnemySprite());
	set_collidable(true);
}

Enemy::~Enemy() {
    SpriteFactory::get_instance().destroyEnemySprite((EnemySprite *)sprite);
}

void Enemy::update() {

	// ..

	Entity::update();
}

void Enemy::render() {

	// ..

	Entity::render();
}

void Enemy::on_collide_callback(Entity * collider) {

    if (collider->get_type_id() == Guy::GUY_TYPE_ID_KEY) {

        struct Sprite::animation_callback cb = {
            ((Guy *)(collider))->get_death_callback(),
            collider
        };

        collider->being_invalidated();

        // remove from scene
        scene->entity_remove(collider);

        // execute animation
        collider->get_sprite()->setAnimation(GuySprite::ANIMATION_DIE, &cb);
    }

    Entity::on_collide_callback(collider);
}

void Enemy::on_bomb_callback() {

    //struct Sprite::animation_callback cb = {
    //    &private_on_enemy_death,
    //    this
    //};

	// remove from scene
	scene->entity_remove(this);

	// apply animation
	//sprite->setAnimation(EnemySprite::ANIMATION_DIE, &cb);

	set_valid(false);

    Entity::on_bomb_callback();
}

StupidEnemy::StupidEnemy(Scene * s) : Enemy(s) {
	set_behavior(new RandomBehavior(*this));
}

StupidEnemy::StupidEnemy(Scene * s, int initial_x, int initial_y) : Enemy(s, initial_x, initial_y) {
	set_behavior(new RandomBehavior(*this));
}

StupidEnemy::~StupidEnemy() {

}

void StupidEnemy::on_collide_callback(Entity * collider) {

    Enemy::on_collide_callback(collider);
}

void StupidEnemy::on_bomb_callback() {

	Enemy::on_bomb_callback();
}

CleverEnemy::CleverEnemy(Scene * s, Entity & target) : Enemy(s) {
	set_behavior(new FollowBehavior(*this, target));
}

CleverEnemy::CleverEnemy(Scene * s, int initial_x, int initial_y, Entity & target) : Enemy(s, initial_x, initial_y) {
	set_behavior(new FollowBehavior(*this, target));
}

CleverEnemy::~CleverEnemy() {

}

void CleverEnemy::set_target(Entity& target)  {
	static_cast<FollowBehavior*>(behavior)->set_target(target);
}

void CleverEnemy::on_collide_callback(Entity * collider) {

    Enemy::on_collide_callback(collider);
}

void CleverEnemy::on_bomb_callback() {

	Enemy::on_bomb_callback();
}
