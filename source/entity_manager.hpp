/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef ENTITY_MANAGER_HPP
#define ENTITY_MANAGER_HPP

#include <vector>

#include "singleton.hpp"
#include "entity.hpp"
#include "entity.hpp"
#include "guy.hpp"
#include "enemy.hpp"
#include "block.hpp"
#include "bomb.hpp"

class EntityManager : public Singleton<EntityManager> {
  public:

    Guy * create_guy(Scene * scene, int x, int y);
    StupidEnemy * create_stupid_enemy(Scene * scene, int x, int y);
    CleverEnemy * create_clever_enemy(Scene * scene, int x, int y, Entity & target);
    BreakableBlock * create_breakable_block(Scene * scene, int x, int y);
    UnbreakableBlock * create_unbreakable_block(Scene * scene, int x, int y);
    Bomb * create_bomb(Scene * scene, int x, int y);

    void clear();

    void update();

    void render();

  private:
    Entity * insert(Entity * entity);

    std::vector<Entity*> entity_list;
};

#endif // ENTITY_MANAGER_HPP
