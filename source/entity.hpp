/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef ENTITY_HPP
#define ENTITY_HPP

#ifndef LIBNDS_HEADER_INCLUDED
#define LIBNDS_HEADER_INCLUDED
#   include <nds.h>
#endif // LIBNDS_HEADER_INCLUDED

#include "sprites.hpp"

class Scene;
class Behavior;

class Entity {

  public:

    enum movement_type {

        MOVE_LEFT,
        MOVE_RIGHT,
        MOVE_UP,
        MOVE_DOWN,
        MOVE_NONE
    };

	static const int default_x = 4;		// tile format
	static const int default_y = 4;
    static const int max_xvel = 2;		// screen format
    static const int max_yvel = 2;
	static const int entity_size = 16;	// screen pixel format

    ~Entity();

    virtual void update();
    virtual void render();

	int get_x(); int get_y();		// tile format

	void set_x(int pos_x); void set_y(int pos_y);		// tile format

	int get_screen_x(); int get_screen_y();	// screen format

	int get_movement();
	void set_movement(int move);

	void set_velocity(int vel_x, int vel_y); // screen format

	bool reached_position();

	int get_type_id();

	void set_scene(Scene * s);

	Scene * get_scene();

	void set_collidable(bool value);

	bool is_collidable();

    void set_sprite(ISprite * s);

    ISprite * get_sprite();

  protected:

    Entity(int id, Scene * s);
	Entity(int id, Scene * s, int initial_x, int initial_y);	// tile format

    int         x;			            // tile format
    int         y;			            // tile format
    int         screen_x;		        // screen format
    int         screen_y;		        // screen format
    int         xvel;		            // screen format
    int         yvel;		            // screen format
	int         movement;		        // Movement index
	int         old_movement;
	Scene *     scene;	        	    // scene which entity belongs to
    Behavior *  behavior;               // behavior of the entity
	bool        collidable;             // check if entitiy is collidable
	int         old_x;                  // tile format
	int         old_y;                  // tile format
	int         position_update_count;
    ISprite *   sprite;
    int         type_id;
    bool        valid;

    void set_behavior(Behavior * b);

    virtual void begin_movement(int movement);

    virtual void end_movement(int movement);

  public:
	virtual void on_collide_callback(Entity * collider);

	virtual void on_bomb_callback();

	void set_valid(bool v);

	bool is_valid();

	void being_invalidated();

  private:
	static const int ENTITY_TYPE_ID_KEY = 0x00;

	void set_position(int new_pos_x, int new_pos_y); // tile format

    bool being_inv;
};

#endif // ENTITY_HPP
