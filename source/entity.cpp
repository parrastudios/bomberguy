/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "entity.hpp"
#include "scene.hpp"
#include "behavior.hpp"
#include "clamp.hpp"
#include "sound.hpp"

#define POSITION_UPDATE_RATE	16
//#define SCREEN_UPDATE_FREQ	1 / POSITION_UPDATE_RATE

Entity::Entity(int id, Scene * s) : x(default_x), y(default_y),
		screen_x(0), screen_y(0),
		xvel(0), yvel(0), movement(MOVE_NONE), old_movement(MOVE_NONE),
		scene(s), behavior(new NullBehavior(*this)), collidable(false),
		old_x(default_x), old_y(default_y), position_update_count(0),
		sprite(NULL), type_id(id), valid(true), being_inv(false) {

    scene->entity_create(this);
}

Entity::Entity(int id, Scene * s, int initial_x, int initial_y) : x(initial_x), y(initial_y),
					screen_x(0), screen_y(0),
					xvel(0), yvel(0), movement(MOVE_NONE), old_movement(MOVE_NONE),
					scene(s), behavior(new NullBehavior(*this)), collidable(false),
					old_x(initial_x), old_y(initial_y), position_update_count(0),
					sprite(NULL), type_id(id), valid(true), being_inv(false) {

	scene->entity_create(this);
}

Entity::~Entity() {
	if (behavior) {
		delete behavior;
	}
}

void Entity::update() {

    if (is_valid()) {
        if (!being_inv) {
            // ..

            // execute behavior
            if (reached_position()) {
                behavior->execute();
            }

            // apply velocity
            set_position(x + xvel, y + yvel);
        }

        if (sprite) {
            sprite->update();
        }
    }
}

void Entity::render() {

}

int Entity::get_x() {
	return x;
}

int Entity::get_y() {
	return y;
}

void Entity::set_x(int pos_x) {
    x = pos_x;
    sprite->setPosition(x * entity_size, y * entity_size);
}

void Entity::set_y(int pos_y) {
    y = pos_y;
    sprite->setPosition(x * entity_size, y * entity_size);
}

int Entity::get_screen_x() {
	return screen_x;
}

int Entity::get_screen_y() {
	return screen_y;
}

int Entity::get_movement() {
	return movement;
}

void Entity::set_movement(int move) {

    if (is_valid()) {
        movement = move;

        if (move == MOVE_UP) {
            set_velocity(0, -1);
        } else if (move == MOVE_DOWN) {
            set_velocity(0, 1);
        } else if (move == MOVE_LEFT) {
            set_velocity(-1, 0);
        } else if (move == MOVE_RIGHT) {
            set_velocity(1, 0);
        } else if (move == MOVE_NONE) {
            set_velocity(0, 0);
        }
    }
}

void Entity::set_velocity(int vel_x, int vel_y) {

    if (is_valid()) {
        xvel = vel_x;
        yvel = vel_y;
    }
}

bool Entity::reached_position() {

    if (is_valid()) {

        if (screen_x >= entity_size || screen_x <= -entity_size) {
            screen_x = 0;
            old_x = x;
        }

        if (screen_y >= entity_size || screen_y <= -entity_size) {
            screen_y = 0;
            old_y = y;
        }

        if (screen_x == 0 && screen_y == 0) {
            if (old_x == x && old_y == y && old_movement != MOVE_NONE && get_movement() == MOVE_NONE) {
                end_movement(old_movement);
            }
            return true;
        }

        return false;
    }

    return true; // never move it
}

int Entity::get_type_id() {
	return type_id;
}

void Entity::set_scene(Scene * s) {
	scene = s;
}

Scene * Entity::get_scene() {
	return scene;
}

void Entity::set_behavior(Behavior * b) {

    if (is_valid()) {
        if (behavior) {
            delete behavior;
        }

        behavior = b;
    }
}

void Entity::set_position(int new_pos_x, int new_pos_y) {

    if (is_valid()) {
        if (position_update_count > POSITION_UPDATE_RATE) {

            if (new_pos_x != get_x() || new_pos_y != get_y()) {

                if (scene->entity_move(this, new_pos_x, new_pos_y)) {

                    // initialize offset screen
                    screen_x = xvel; screen_y = yvel;

                    // set old position of the entity
                    old_x = x; old_y = y;

                    // set position of the entity
                    x = new_pos_x; y = new_pos_y;

                    // callback to start movement
                    if (old_movement != get_movement()) {
                        begin_movement(get_movement());
                    }
                }

                set_movement(MOVE_NONE);
            }

            position_update_count = 0;
        }

        // set screen values
        if (screen_x != 0 || screen_y != 0) {
            int update_x = (screen_x > 0) ? 1 : -(screen_x < 0);
            int update_y = (screen_y > 0) ? 1 : -(screen_y < 0);

            if (ClampInt::value(screen_x, screen_x + update_x, -entity_size, entity_size)) {
                screen_x = 0;
                old_x = x;
            }

            if (ClampInt::value(screen_y, screen_y + update_y, -entity_size, entity_size)) {
                screen_y = 0;
                old_y = y;
            }

            if (sprite) {
                sprite->setPosition(((old_x * entity_size) + screen_x), ((old_y * entity_size) + screen_y));
            }
        }

        ++position_update_count;
    }
}

void Entity::set_collidable(bool value) {

    if (is_valid()) {
        collidable = value;
    }
}

bool Entity::is_collidable() {

    if (is_valid()) {
        return collidable;
    }

    return false;
}

void Entity::on_collide_callback(Entity * collider) {

    if (is_valid()) {
        SoundManager::get_instance().play(SoundManager::SOUND_COLLISION);
    }
}

void Entity::on_bomb_callback() {

    if (is_valid()) {

    }
}

void Entity::set_valid(bool v) {
    valid = v;

    if (valid) {
        sprite->hide(false);

    } else {
        sprite->hide(true);
        being_inv = false;
    }
}

bool Entity::is_valid() {
    return valid;
}

void Entity::being_invalidated() {
    being_inv = true;
}

ISprite * Entity::get_sprite() {
    return sprite;
}

void Entity::set_sprite(ISprite * s) {

    if (is_valid()) {
        sprite = s;

        if (sprite) {
            sprite->setPosition(((old_x * entity_size) + screen_x), ((old_y * entity_size) + screen_y));
        }
    }
}

void Entity::begin_movement(int movement) {

    old_movement = movement;
}

void Entity::end_movement(int movement) {

    old_movement = MOVE_NONE;
}
