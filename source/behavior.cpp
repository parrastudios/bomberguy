/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "behavior.hpp"
#include "randomizer.hpp"
#include "guy.hpp"

Behavior::Behavior(Entity & ent) : entity(ent) {

}

Behavior::~Behavior() {

}

NullBehavior::NullBehavior(Entity & ent) : Behavior(ent) {

}

NullBehavior::~NullBehavior() {

}

void NullBehavior::execute() {
    entity.set_movement(Entity::MOVE_NONE);
}

RandomBehavior::RandomBehavior(Entity & ent) : Behavior(ent), prev_movement(Entity::MOVE_NONE) {

}

RandomBehavior::~RandomBehavior() {

}

void RandomBehavior::execute() {

	int movement = randomize_movement();

    if (movement != Entity::MOVE_NONE) {
		entity.set_movement(movement);
    }
}

int RandomBehavior::randomize_movement() {
	return Randomizer::get_instance().random(Entity::MOVE_LEFT, Entity::MOVE_NONE);
}

FollowBehavior::FollowBehavior(Entity & ent, Entity & tar) : Behavior(ent), target(tar) {

}

FollowBehavior::~FollowBehavior() {

}

void FollowBehavior::execute() {
	// ..
}

void FollowBehavior::set_target(Entity & t) {
	target = t;
}

UserInputBehavior::UserInputBehavior(Entity & ent) : Behavior(ent) {

}

UserInputBehavior::~UserInputBehavior() {

}

void UserInputBehavior::execute() {

	int keysh, keydw;

    scanKeys();

    keydw = keysDown();

    if (keydw & KEY_A) {
        if (entity.get_type_id() == Guy::GUY_TYPE_ID_KEY) {
                Guy * guy = (Guy *)&entity;
                guy->create_bomb();
        }
    }

    keysh = keysHeld();

    // check if UP is pressed
    if (keysh & KEY_UP) {
        entity.set_movement(Entity::MOVE_UP);
        return;
    }

    // check if DOWN is pressed
    if (keysh & KEY_DOWN) {
        entity.set_movement(Entity::MOVE_DOWN);
        return;
    }

    // check if LEFT is pressed
    if (keysh & KEY_LEFT) {
        entity.set_movement(Entity::MOVE_LEFT);
        return;
    }

    // check if RIGHT is pressed
    if (keysh & KEY_RIGHT) {
        entity.set_movement(Entity::MOVE_RIGHT);
        return;
    }

    entity.set_movement(Entity::MOVE_NONE);
}
