/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GFX_DATA_HPP
#define GFX_DATA_HPP

// BG
#include "gfx_grass.h"
#include "gfx_block.h"
#include "gfx_brick.h"

#include "gfx_fire_middle.h"
#include "gfx_fire_down_hand.h"
#include "gfx_fire_up_hand.h"
#include "gfx_fire_left_hand.h"
#include "gfx_fire_right_hand.h"
#include "gfx_fire_vert_arm.h"
#include "gfx_fire_hori_arm.h"

// OBJ
#include "gfx_guy_down_0.h"
#include "gfx_guy_down_1.h"
#include "gfx_guy_down_2.h"
#include "gfx_guy_up_0.h"
#include "gfx_guy_up_1.h"
#include "gfx_guy_up_2.h"
#include "gfx_guy_left_0.h"
#include "gfx_guy_left_1.h"
#include "gfx_guy_left_2.h"
#include "gfx_guy_right_0.h"
#include "gfx_guy_right_1.h"
#include "gfx_guy_right_2.h"
#include "gfx_guy_die_0.h"
#include "gfx_guy_die_1.h"
#include "gfx_guy_die_2.h"
#include "gfx_guy_die_3.h"
#include "gfx_guy_die_4.h"

#include "gfx_bomb_0.h"
#include "gfx_bomb_1.h"
#include "gfx_bomb_2.h"

#include "gfx_enemy_0.h"
#include "gfx_enemy_1.h"
#include "gfx_enemy_2.h"
#include "gfx_enemy_3.h"

#endif // GFX_DATA_HPP
