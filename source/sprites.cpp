/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "sprites.hpp"
#include "scene.hpp"

#include "gfx_data.hpp"

// BACKGROUND

// tile map of the bg
const int bgBlocks[] =
{
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const int bgPals[] =
{
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

const int blockTypePal[] =
{
	0,	// GRASS_BLOCK_ID
	1,	// HARD_BRICK_BLOCK_ID
	1,	// SOFT_BRICK_BLOCK_ID

	2,	// FIRE_MIDDLE_BLOCK_ID
	2,	// FIRE_DOWN_BLOCK_ID
	2,	// FIRE_UP_BLOCK_ID
	2,	// FIRE_LEFT_BLOCK_ID
	2,	// FIRE_RIGHT_BLOCK_ID
	2,	// FIRE_VERT_BLOCK_ID
	2,	// FIRE_HORI_BLOCK_ID
	2	// FIRE_BLANK_BLOCK_ID
};

void Sprites::setup_graphics()
{

	irqEnable( IRQ_VBLANK );

	// set mode 0 | enable BG 0 | enable BG 1 | enable sprites | 1D layout
	videoSetMode
	(
		MODE_0_2D |
		DISPLAY_BG0_ACTIVE | DISPLAY_BG1_ACTIVE |
		DISPLAY_SPR_ACTIVE |
		DISPLAY_SPR_1D_LAYOUT
	);

	vramSetBankE( VRAM_E_MAIN_BG );
    vramSetBankF( VRAM_F_MAIN_SPRITE );

    // BACKGROUND

    // copy BG graphics to VRAM
    dmaCopyHalfWords( 3, gfx_grassTiles, tile2bgram(0), gfx_grassTilesLen );
    dmaCopyHalfWords( 3, gfx_blockTiles, tile2bgram(4), gfx_blockTilesLen );
    dmaCopyHalfWords( 3, gfx_brickTiles, tile2bgram(8), gfx_brickTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_middleTiles, tile2bgram(12), gfx_fire_middleTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_down_handTiles, tile2bgram(16), gfx_fire_down_handTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_up_handTiles, tile2bgram(20), gfx_fire_up_handTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_left_handTiles, tile2bgram(24), gfx_fire_left_handTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_right_handTiles, tile2bgram(28), gfx_fire_right_handTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_vert_armTiles, tile2bgram(32), gfx_fire_vert_armTilesLen );
    dmaCopyHalfWords( 3, gfx_fire_hori_armTiles, tile2bgram(36), gfx_fire_hori_armTilesLen );
    // blank block for the fire bg
    for(int i=0; i<16; i++) tile2bgram(40)[i] = 0;
    for(int i=0; i<16; i++) tile2bgram(41)[i] = 0;
    for(int i=0; i<16; i++) tile2bgram(42)[i] = 0;
    for(int i=0; i<16; i++) tile2bgram(43)[i] = 0;

    // copy BG pal to VRAM
    dmaCopyHalfWords( 3, gfx_grassPal, pal2bgram(0), gfx_grassPalLen );
    dmaCopyHalfWords( 3, gfx_blockPal, pal2bgram(1), gfx_blockPalLen );
    dmaCopyHalfWords( 3, gfx_fire_middlePal, pal2bgram(2), gfx_fire_middlePalLen );
    // block and brick share the same palette

    // configure the BG settings registers
    // with this line we indicate that the bg tilemap is
    // stored in the second (1) base4 block of the VRAM
    // each base block occupies 2KB
    // we are using the second because the first is used
    // by the bg tiles (grass)
    REG_BG0CNT = BG_MAP_BASE(7) | 1;	// priority 1
    REG_BG1CNT = BG_MAP_BASE(8);		// priority 0

    // fill the tile map (all the tiles are the same)

    for(int i=0; i<256; i++)
    {

		const int x = i%16;
		const int y = i/16;
		int pal = bgPals[i];
		int block = bgBlocks[i];

		// top left
		bg_map[ 2*x+2*y*32 ] =
		pal << 12 |							// select palette
		(block*4);		// select tile

		// top right
		bg_map[ 2*x+2*y*32+1 ] =
		pal << 12 |							// select palette
		(block*4+1);	// select tile

		// bot left
		bg_map[ 2*x+2*y*32+32 ] =
		pal << 12 |							// select palette
		(block*4+2);	// select tile

		// bot right
		bg_map[ 2*x+2*y*32+32+1 ] =
		pal << 12 |							// select palette
		(block*4+3);	// select tile

	}

	// fill the tilemap of the fire bg
	for(int i=0; i<1024; i++)
    {

		const int firePal = blockTypePal[FIRE_MIDDLE_BLOCK_ID];
		fire_bg_map[i] = 40 | firePal << 12;

	}



    // SPRITES

    // disable all sprites
    const int spritesCount = 128;
    for(int i=0; i<spritesCount; i++)
    {
		sprites[i].attr0 = ATTR0_DISABLED;
	}

    // load guy tile data
    // down 0
    dmaCopyHalfWords
    (
		3,
		gfx_guy_down_0Tiles,
		tile2objram( 4*SPRITE_GUY_DOWN_0 ),
		gfx_guy_down_0TilesLen
	);

    // down 1
    dmaCopyHalfWords
    (
		3,
		gfx_guy_down_1Tiles,
		tile2objram( 4*SPRITE_GUY_DOWN_1 ),
		gfx_guy_down_1TilesLen
	);

    // down 2
    dmaCopyHalfWords
    (
		3,
		gfx_guy_down_2Tiles,
		tile2objram( 4*SPRITE_GUY_DOWN_2 ),
		gfx_guy_down_2TilesLen
	);

	// up 0
    dmaCopyHalfWords
    (
		3,
		gfx_guy_up_0Tiles,
		tile2objram( 4*SPRITE_GUY_UP_0 ),
		gfx_guy_up_0TilesLen
	);

	// up 1
    dmaCopyHalfWords
    (
		3,
		gfx_guy_up_1Tiles,
		tile2objram( 4*SPRITE_GUY_UP_1 ),
		gfx_guy_up_1TilesLen
	);

	// up 2
    dmaCopyHalfWords
    (
		3,
		gfx_guy_up_2Tiles,
		tile2objram( 4*SPRITE_GUY_UP_2 ),
		gfx_guy_up_2TilesLen
	);

	// left 0
    dmaCopyHalfWords
    (
		3,
		gfx_guy_left_0Tiles,
		tile2objram( 4*SPRITE_GUY_LEFT_0 ),
		gfx_guy_left_0TilesLen
	);

    // left 1
    dmaCopyHalfWords
    (
		3,
		gfx_guy_left_1Tiles,
		tile2objram( 4*SPRITE_GUY_LEFT_1 ),
		gfx_guy_left_1TilesLen
	);

	// left 2
    dmaCopyHalfWords
    (
		3,
		gfx_guy_left_2Tiles,
		tile2objram( 4*SPRITE_GUY_LEFT_2 ),
		gfx_guy_left_2TilesLen
	);

	// right 0
    dmaCopyHalfWords
    (
		3,
		gfx_guy_right_0Tiles,
		tile2objram( 4*SPRITE_GUY_RIGHT_0 ),
		gfx_guy_right_0TilesLen
	);

	// right 1
    dmaCopyHalfWords
    (
		3,
		gfx_guy_right_1Tiles,
		tile2objram( 4*SPRITE_GUY_RIGHT_1 ),
		gfx_guy_right_1TilesLen
	);

	// right 2
    dmaCopyHalfWords
    (
		3,
		gfx_guy_right_2Tiles,
		tile2objram( 4*SPRITE_GUY_RIGHT_2 ),
		gfx_guy_right_2TilesLen
	);

	// die 0
    dmaCopyHalfWords
    (
		3,
		gfx_guy_die_0Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_0 ),
		gfx_guy_die_0TilesLen
	);

	// die 1
    dmaCopyHalfWords
    (
		3,
		gfx_guy_die_1Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_1 ),
		gfx_guy_die_1TilesLen
	);

	// die 2
    dmaCopyHalfWords
    (
		3,
		gfx_guy_die_2Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_2 ),
		gfx_guy_die_2TilesLen
	);

	// die 3
    dmaCopyHalfWords
    (
		3,
		gfx_guy_die_3Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_3 ),
		gfx_guy_die_3TilesLen
	);

	// die 4
    dmaCopyHalfWords
    (
		3,
		gfx_guy_die_4Tiles,
		tile2objram( 4*SPRITE_GUY_DIE_4 ),
		gfx_guy_die_4TilesLen
	);

	// bomb 0
	dmaCopyHalfWords
    (
		3,
		gfx_bomb_0Tiles,
		tile2objram( 4*SPRITE_BOMB_0 ),
		gfx_bomb_0TilesLen
	);

	// bomb 1
	dmaCopyHalfWords
    (
		3,
		gfx_bomb_1Tiles,
		tile2objram( 4*SPRITE_BOMB_1 ),
		gfx_bomb_1TilesLen
	);

	// bomb 2
	dmaCopyHalfWords
    (
		3,
		gfx_bomb_2Tiles,
		tile2objram( 4*SPRITE_BOMB_2 ),
		gfx_bomb_2TilesLen
	);

    // enemy 0
    dmaCopyHalfWords
    (
		3,
		gfx_enemy_0Tiles,
		tile2objram( 4*SPRITE_ENEMY_0 ),
		gfx_enemy_0TilesLen
	);

	// enemy 1
    dmaCopyHalfWords
    (
		3,
		gfx_enemy_1Tiles,
		tile2objram( 4*SPRITE_ENEMY_1 ),
		gfx_enemy_1TilesLen
	);

	// enemy 2
    dmaCopyHalfWords
    (
		3,
		gfx_enemy_2Tiles,
		tile2objram( 4*SPRITE_ENEMY_2 ),
		gfx_enemy_2TilesLen
	);

	// enemy 3
    dmaCopyHalfWords
    (
		3,
		gfx_enemy_3Tiles,
		tile2objram( 4*SPRITE_ENEMY_3 ),
		gfx_enemy_3TilesLen
	);

    // load guy palette (all the frames of the guy share the same palette)
    // also the bomb is using the same palette
    // also the enemy is using the same palette
    dmaCopyHalfWords
    (
		3,
		gfx_guy_down_0Pal,
		pal2objram( PALETTE_GUY ),
		gfx_guy_down_0PalLen
    );

}

void Sprites::setBgBlock(int x, int y, BlockType blockType)
{

	const int pal = blockTypePal[blockType];
	const int block = blockType;

	// top left
	bg_map[ 2*x+2*y*32 ] =
	pal << 12 |							// select palette
	(block*4);		// select tile

	// top right
	bg_map[ 2*x+2*y*32+1 ] =
	pal << 12 |							// select palette
	(block*4+1);	// select tile

	// bot left
	bg_map[ 2*x+2*y*32+32 ] =
	pal << 12 |							// select palette
	(block*4+2);	// select tile

	// bot right
	bg_map[ 2*x+2*y*32+32+1 ] =
	pal << 12 |							// select palette
	(block*4+3);	// select tile

}

void Sprites::setFireBgBlock(int x, int y, BlockType blockType)
{

	const int pal = blockTypePal[blockType];
	int block = blockType;

	// top left
	fire_bg_map[ 2*x+2*y*32 ] =
	pal << 12 |							// select palette
	(block*4);		// select tile

	// top right
	fire_bg_map[ 2*x+2*y*32+1 ] =
	pal << 12 |							// select palette
	(block*4+1);	// select tile

	// bot left
	fire_bg_map[ 2*x+2*y*32+32 ] =
	pal << 12 |							// select palette
	(block*4+2);	// select tile

	// bot right
	fire_bg_map[ 2*x+2*y*32+32+1 ] =
	pal << 12 |							// select palette
	(block*4+3);	// select tile

}

void Sprites::drawFireCross
(
	s16 centerX, s16 centerY,
	s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
)
{

	setFireBgBlock( centerX, centerY, FIRE_MIDDLE_BLOCK_ID );

	if( downLimit != centerY )
	{

		setFireBgBlock( centerX, downLimit, FIRE_DOWN_BLOCK_ID );

		downLimit--;
		while( downLimit != centerY)
		{
			setFireBgBlock( centerX, downLimit, FIRE_VERT_BLOCK_ID);
			downLimit--;
		}

	}

	if ( upLimit != centerY )
	{

		setFireBgBlock( centerX, upLimit, FIRE_UP_BLOCK_ID );

		upLimit++;
		while( upLimit != centerY)
		{
			setFireBgBlock( centerX, upLimit, FIRE_VERT_BLOCK_ID);
			upLimit++;
		}

	}

	if ( leftLimit != centerX )
	{

		setFireBgBlock( leftLimit, centerY, FIRE_LEFT_BLOCK_ID );

		leftLimit++;
		while( leftLimit != centerX)
		{
			setFireBgBlock( leftLimit, centerY, FIRE_HORI_BLOCK_ID);
			leftLimit++;
		}

	}

	if ( rightLimit != centerX )
	{

		setFireBgBlock( rightLimit, centerY, FIRE_RIGHT_BLOCK_ID );

		rightLimit--;
		while( rightLimit != centerX)
		{
			setFireBgBlock( rightLimit, centerY, FIRE_HORI_BLOCK_ID);
			rightLimit--;
		}

	}


}

void Sprites::clearFireCross
(
	s16 centerX, s16 centerY,
	s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
)
{

	setFireBgBlock( centerX, centerY, FIRE_BLANK_BLOCK_ID );

	if( downLimit != centerY )
	{

		setFireBgBlock( centerX, downLimit, FIRE_BLANK_BLOCK_ID );

		downLimit--;
		while( downLimit != centerY)
		{
			setFireBgBlock( centerX, downLimit, FIRE_BLANK_BLOCK_ID );
			downLimit--;
		}

	}

	if ( upLimit != centerY )
	{

		setFireBgBlock( centerX, upLimit, FIRE_BLANK_BLOCK_ID );

		upLimit++;
		while( upLimit != centerY)
		{
			setFireBgBlock( centerX, upLimit, FIRE_BLANK_BLOCK_ID );
			upLimit++;
		}

	}

	if ( leftLimit != centerX )
	{

		setFireBgBlock( leftLimit, centerY, FIRE_BLANK_BLOCK_ID );

		leftLimit++;
		while( leftLimit != centerX)
		{
			setFireBgBlock( leftLimit, centerY, FIRE_BLANK_BLOCK_ID );
			leftLimit++;
		}

	}

	if ( rightLimit != centerX )
	{

		setFireBgBlock( rightLimit, centerY, FIRE_BLANK_BLOCK_ID );

		rightLimit--;
		while( rightLimit != centerX)
		{
			setFireBgBlock( rightLimit, centerY, FIRE_BLANK_BLOCK_ID );
			rightLimit--;
		}

	}

}


ISprite::ISprite() {
	this->slot = -1;
}

ISprite::ISprite(int slot) {
	this->slot = slot;
}

ISprite::~ISprite() {

}

int ISprite::getSlot() const
{
	return slot;
}

Sprite::Sprite(int slot) : ISprite(slot)
{
	spriteEntry = &sprites[slot];
	spriteEntry->attr1&= ~(0x3<<14);
	spriteEntry->attr1 |= (0x1<<14);		// set size flag to 16x16

}

void Sprite::setSprite(int sprite)
{

	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= sprite;

}

void Sprite::setPalette(int palette)
{

	spriteEntry->attr2 &= ~(0xF<<12);
	spriteEntry->attr2 |= (palette<<12);

}

void Sprite::setPriority(int priority)
{

	spriteEntry->attr2 &= ~(0x3<<10);
	spriteEntry->attr2 |= (priority<<10);

}

void Sprite::setPosition(s16 x, s16 y)
{

	y &= 0xFF;
	spriteEntry->attr0 &= ~(0xFF);		// set last byte to 0
	spriteEntry->attr0 |= y;

	x &= 0x1FF;
	spriteEntry->attr1 &= ~(0x1FF);
	spriteEntry->attr1 |= x;
}

void Sprite::setAnimation(int animation, struct animation_callback * callback) {

    if (callback != NULL) {
        anim_callback.object = callback->object;
        anim_callback.func = callback->func;
    } else {
        anim_callback.object = NULL;
        anim_callback.func = NULL;
    }
}

void Sprite::hide(bool b)
{

	// hide
	if(b)
	{
		spriteEntry->attr0 &= ~(1<<8);		// set bit 8 to 0
		spriteEntry->attr0 |= (1<<9);		// set bit 9 to 1
	}

	// show
	else
	{
		spriteEntry->attr0 &= ~(1<<8);		// set bit 8 to 0
		spriteEntry->attr0 &= ~(1<<9);		// set bit 9 to 0
	}

}


// GUY

GuySprite::GuySprite(int slot)
:
Sprite(slot)
{

	animationStep = 0;
	animation = ANIMATION_STAND_DOWN;
	setSprite(SPRITE_GUY_DOWN_0);

}

void GuySprite::setSprite(int sprite)
{

	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;

}

void GuySprite::setAnimation(int animation, struct animation_callback * callback)
{

	this->animation = animation;
	frameCount = 0;
	animationStep = 0;

	if( animation == ANIMATION_STAND_DOWN )
	{
		setSprite(SPRITE_GUY_DOWN_0);
	}

	else if( animation == ANIMATION_STAND_UP )
	{
		setSprite(SPRITE_GUY_UP_0);
	}

	else if( animation == ANIMATION_STAND_LEFT )
	{
		setSprite(SPRITE_GUY_LEFT_0);
	}

	else if( animation == ANIMATION_STAND_RIGHT )
	{
		setSprite(SPRITE_GUY_RIGHT_0);
	}


	else if( animation == ANIMATION_WALKING_DOWN )
	{
		setSprite(SPRITE_GUY_DOWN_0);
	}

	else if( animation == ANIMATION_WALKING_UP )
	{
		setSprite(SPRITE_GUY_UP_0);
	}

	else if( animation == ANIMATION_WALKING_LEFT )
	{
		setSprite(SPRITE_GUY_LEFT_0);
	}

	else if( animation == ANIMATION_WALKING_RIGHT )
	{
		setSprite(SPRITE_GUY_RIGHT_0);
	}

	else if( animation == ANIMATION_DIE )
	{
		setSprite(SPRITE_GUY_DIE_0);
	}

    Sprite::setAnimation(animation, callback);
}

void GuySprite::update()
{

	if( animation == ANIMATION_STAND_DOWN )
	{
		// nothing to do here
	}

	else if( animation == ANIMATION_STAND_UP )
	{
		// nothing to do here
	}

	else if( animation == ANIMATION_STAND_LEFT )
	{
		// nothing to do here
	}

	else if( animation == ANIMATION_STAND_RIGHT )
	{
		// nothing to do here
	}

	// animation walking down
	else if( animation == ANIMATION_WALKING_DOWN )
	{

		if(frameCount >= 20)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_DOWN_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_DOWN_0;
			}
			else if( animationStep == 3 )
			{
				sprite = SPRITE_GUY_DOWN_2;
			}
			else if( animationStep == 4 )
			{
				sprite = SPRITE_GUY_DOWN_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

	// animation walking up
	else if( animation == ANIMATION_WALKING_UP )
	{

		if(frameCount >= 20)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_UP_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_UP_0;
			}
			else if( animationStep == 3 )
			{
				sprite = SPRITE_GUY_UP_2;
			}
			else if( animationStep == 4 )
			{
				sprite = SPRITE_GUY_UP_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

	// animation walking left
	else if( animation == ANIMATION_WALKING_LEFT )
	{

		if(frameCount >= 20)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_LEFT_2;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_LEFT_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

	// animation walking right
	else if( animation == ANIMATION_WALKING_RIGHT )
	{

		if(frameCount >= 20)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_RIGHT_2;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_RIGHT_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}


	// animation die
	else if( animation == ANIMATION_DIE )
	{

		if(frameCount >= 20)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_GUY_DIE_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_GUY_DIE_2;
			}
			else if( animationStep == 3 )
			{
				sprite = SPRITE_GUY_DIE_3;
			}
			else if( animationStep == 4 )
			{
				sprite = SPRITE_GUY_DIE_4;
			}
			else if( animationStep == 5 )
			{
				sprite = SPRITE_GUY_DIE_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

}


// BOMB

BombSprite::BombSprite(int slot)
:
Sprite(slot)
{

	animationStep = 0;
	animation = ANIMATION_DEFAULT;
	setSprite(SPRITE_BOMB_0);

}

void BombSprite::setSprite(int sprite)
{

	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;

}

void BombSprite::setAnimation(int animation, struct animation_callback * callback)
{

	this->animation = animation;

	if( animation == ANIMATION_DEFAULT )
	{
		setSprite(SPRITE_BOMB_0);
	}

	else if( animation == ANIMATION_EXPLODE )
	{
		setSprite(SPRITE_BOMB_2);
	}

    Sprite::setAnimation(animation, callback);
}

void BombSprite::update()
{

	// default animation of the bomb
	if( animation == ANIMATION_DEFAULT )
	{

		if(frameCount >= 30)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_BOMB_2;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_BOMB_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}
	// explode animation
	if( animation == ANIMATION_EXPLODE )
	{

		if(frameCount >= 30)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_BOMB_0;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_BOMB_1;
				animationStep = 1;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

}


// ENEMY
EnemySprite::EnemySprite(int slot)
:
Sprite(slot)
{

	animationStep = 0;
	animation = ANIMATION_DEFAULT;
	setSprite(SPRITE_ENEMY_0);

}

void EnemySprite::setSprite(int sprite)
{

	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;
}

void EnemySprite::setAnimation(int animation, struct animation_callback * callback)
{

	this->animation = animation;

	if( animation == ANIMATION_DEFAULT )
	{
		setSprite(SPRITE_ENEMY_0);
	}

	else if( animation == ANIMATION_DIE )
	{
		setSprite(SPRITE_ENEMY_2);
	}

    Sprite::setAnimation(animation, callback);
}

void EnemySprite::update()
{

	// default animation of the enemy
	if( animation == ANIMATION_DEFAULT )
	{

		if(frameCount >= 30)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_ENEMY_1;
			}
			else if( animationStep == 2 )
			{
				sprite = SPRITE_ENEMY_0;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

	// die animation
	if( animation == ANIMATION_DIE )
	{

		if(frameCount >= 50)	// change frame
		{

			animationStep++;
			sprite = animationStep;

			if( animationStep == 1 )
			{
				sprite = SPRITE_ENEMY_3;
				animationStep = 0;

				if (anim_callback.func) {
                    anim_callback.func(anim_callback.object);
				}
			}

			setSprite(sprite);
			frameCount = 0;

		}

		frameCount++;

	}

}

BlockSpriteFake::BlockSpriteFake(int slot) : block_type(slot), bg_x(-1), bg_y(-1) {

}

void BlockSpriteFake::setPosition(s16 x, s16 y) {
    if (bg_x != -1 && bg_y != -1) {
        hide(true);
    }

    if ((x >= 0 && x < MAP_TILE_SIZE_WIDTH) && (y >= 0 && y < MAP_TILE_SIZE_HEIGHT)) {
        bg_x = x;
        bg_y = y;

        if (block_type != -1) {
            Sprites::get_instance().setBgBlock(bg_x, bg_y, (Sprites::BlockType)block_type);
        }
    }
}

void BlockSpriteFake::hide(bool b) {
    if (b == true) {
        Sprites::get_instance().setBgBlock(bg_x, bg_y, Sprites::GRASS_BLOCK_ID);
    } else {
        if (block_type != -1) {
            Sprites::get_instance().setBgBlock(bg_x, bg_y, (Sprites::BlockType)block_type);
        }
    }
}

void BlockSpriteFake::setSprite(int block_type) {

    this->block_type = block_type;
}

void BlockSpriteFake::setPalette(int palette) {

    // todo ?
}

void BlockSpriteFake::setAnimation(int animation, struct animation_callback * callback) {

    // nothing
}

int BlockSpriteFake::getSlot() const {
    return -1;
}

void BlockSpriteFake::setPriority(int priority) {

    // nothing
}

BreakableBlockSprite::BreakableBlockSprite(int slot) : BlockSpriteFake(Sprites::SOFT_BRICK_BLOCK_ID) {

}

void BreakableBlockSprite::setPosition(s16 x, s16 y) {
    BlockSpriteFake::setPosition(x, y);
}

void BreakableBlockSprite::hide(bool b) {
    BlockSpriteFake::hide(b);
}

void BreakableBlockSprite::setAnimation(int animation, struct animation_callback * callback) {
    BlockSpriteFake::setAnimation(animation, callback);
}

void BreakableBlockSprite::setSprite(int block_type) {
    BlockSpriteFake::setSprite(block_type);
}

void BreakableBlockSprite::setPalette(int palette) {
    BlockSpriteFake::setPalette(palette);
}

void BreakableBlockSprite::setPriority(int priority){
    BlockSpriteFake::setPriority(priority);
}

BreakableBlockSpriteAnim::BreakableBlockSpriteAnim(int slot) : Sprite(slot) {

	animationStep = 0;
	//animation = ANIMATION_DEFAULT;
	//setSprite(SPRITE_ENEMY_0);
}

void BreakableBlockSpriteAnim::setSprite(int sprite)
{

	this->sprite = sprite;
	spriteEntry->attr2 &= ~(0x1FF);
	spriteEntry->attr2 |= 4*sprite;
	frameCount = 0;
}

void BreakableBlockSpriteAnim::setAnimation(int animation, struct animation_callback * callback)
{
    // todo
    // ...

    Sprite::setAnimation(animation, callback);
}

void BreakableBlockSpriteAnim::update()
{
    // todo
}

UnbreakableBlockSprite::UnbreakableBlockSprite(int slot) : BlockSpriteFake(Sprites::HARD_BRICK_BLOCK_ID) {

}

void UnbreakableBlockSprite::setPosition(s16 x, s16 y) {
    BlockSpriteFake::setPosition(x, y);
}

void UnbreakableBlockSprite::hide(bool b) {
    BlockSpriteFake::hide(b);
}

void UnbreakableBlockSprite::setAnimation(int animation, struct animation_callback * callback) {
    BlockSpriteFake::setAnimation(animation, callback);
}

void UnbreakableBlockSprite::setSprite(int block_type) {
    BlockSpriteFake::setSprite(block_type);
}

void UnbreakableBlockSprite::setPalette(int palette) {
    BlockSpriteFake::setPalette(palette);
}

void UnbreakableBlockSprite::setPriority(int priority){
    BlockSpriteFake::setPriority(priority);
}

SpriteFactory::SpriteFactory()
:freeSlots(128)
{

	for(int i=0; i<128; i++)
	{
		freeSlots[i] = 127-i;	// [127 .. 0]
	}

}

GuySprite* SpriteFactory::createGuySprite()
{

	int slot = freeSlots.back();
	freeSlots.pop_back();

	GuySprite* guySprite = new GuySprite(slot);
	guySprite->hide(false);

	return guySprite;

}

EnemySprite* SpriteFactory::createEnemySprite()
{

	int slot = freeSlots.back();
	freeSlots.pop_back();

	EnemySprite* enemySprite = new EnemySprite(slot);
	enemySprite->hide(false);

	return enemySprite;

}

BombSprite* SpriteFactory::createBombSprite()
{

	int slot = freeSlots.back();
	freeSlots.pop_back();

	BombSprite* bombSprite = new BombSprite(slot);
	bombSprite->hide(false);

	return bombSprite;

}

BreakableBlockSprite * SpriteFactory::createBreakableBlockSprite() {
    return new BreakableBlockSprite(-1);
}

BreakableBlockSpriteAnim * SpriteFactory::createBreakableBlockSpriteAnim() {
	int slot = freeSlots.back();

	freeSlots.pop_back();

	BreakableBlockSpriteAnim * break_block_anim = new BreakableBlockSpriteAnim(slot);

	break_block_anim->hide(false);

	return break_block_anim;
}

UnbreakableBlockSprite * SpriteFactory::createUnbreakableBlockSprite() {
    return new UnbreakableBlockSprite(-1);
}

void SpriteFactory::destroyGuySprite( GuySprite* guySprite )
{

	int slot = guySprite->getSlot();
	freeSlots.push_back(slot);
	guySprite->hide(true);
	delete guySprite;

}

void SpriteFactory::destroyEnemySprite( EnemySprite* enemySprite )
{

	int slot = enemySprite->getSlot();
	freeSlots.push_back(slot);
	enemySprite->hide(true);
	delete enemySprite;

}

void SpriteFactory::destroyBombSprite( BombSprite* bombSprite )
{

	int slot = bombSprite->getSlot();
	freeSlots.push_back(slot);
	bombSprite->hide(true);
	delete bombSprite;

}

void SpriteFactory::destroyBreakableBlockSprite(BreakableBlockSprite * break_block) {
    delete break_block;
}

void SpriteFactory::destroyBreakableBlockAnimSprite(BreakableBlockSpriteAnim * break_block_anim) {
	int slot = break_block_anim->getSlot();

	freeSlots.push_back(slot);

	break_block_anim->hide(true);

	delete break_block_anim;
}

void SpriteFactory::destroyUnbreakableBlockSprite(UnbreakableBlockSprite * unbreak_block) {
    delete unbreak_block;
}

FireCross::FireCross() {

}

FireCross::FireCross
(
	s16 centerX, s16 centerY,
	s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
)
{

	this->centerX = centerX;
	this->centerY = centerY;
	this->downLimit = downLimit;
	this->upLimit = upLimit;
	this->leftLimit = leftLimit;
	this->rightLimit = rightLimit;

}

void FireCross::draw()
{
	Sprites::get_instance().drawFireCross
	(
		centerX, centerY,
		downLimit, upLimit, leftLimit, rightLimit
	);
}

void FireCross::clean()
{
	Sprites::get_instance().clearFireCross
	(
		centerX, centerY,
		downLimit, upLimit, leftLimit, rightLimit
	);
}
