/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "entity_manager.hpp"

Guy * EntityManager::create_guy(Scene * scene, int x, int y) {
    return (Guy *)insert(new Guy(scene, x, y));
}

StupidEnemy * EntityManager::create_stupid_enemy(Scene * scene, int x, int y) {
    return (StupidEnemy *)insert(new StupidEnemy(scene, x, y));
}

CleverEnemy * EntityManager::create_clever_enemy(Scene * scene, int x, int y, Entity & target) {
    return (CleverEnemy *)insert(new CleverEnemy(scene, x, y, target));
}

BreakableBlock * EntityManager::create_breakable_block(Scene * scene, int x, int y) {
    return (BreakableBlock *)insert(new BreakableBlock(scene, x, y));
}

UnbreakableBlock * EntityManager::create_unbreakable_block(Scene * scene, int x, int y) {
    return (UnbreakableBlock *)insert(new UnbreakableBlock(scene, x, y));
}

Bomb * EntityManager::create_bomb(Scene * scene, int x, int y) {
    return (Bomb *)insert(new Bomb(scene, x, y));
}

void EntityManager::clear() {
    entity_list.clear();
}

void EntityManager::update() {
    std::vector<Entity*>::iterator it = entity_list.begin();

    while (it != entity_list.end()) {
        (*it)->update();
        ++it;
    }
}

void EntityManager::render() {
    std::vector<Entity*>::iterator it = entity_list.begin();

    while (it != entity_list.end()) {
        (*it)->render();
        ++it;
    }
}

Entity * EntityManager::insert(Entity * entity) {
    entity_list.push_back(entity);

    return entity;
}
