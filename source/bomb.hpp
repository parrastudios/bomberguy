/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef BOMB_HPP
#define BOMB_HPP

#include "entity.hpp"

class Guy;

class Bomb : public Entity {

  public:

	static const int BOMB_TYPE_ID_KEY = 0x06;

	Bomb(Scene * s);
    Bomb(Scene * s, int initial_x, int initial_y);
    virtual ~Bomb();

	virtual void update();

	virtual void render();

    virtual void on_collide_callback(Entity * collider);

    virtual void on_bomb_callback();

    void countdown();

    void explode();

    int get_range();

    void set_range(int r);

    void attach_to_guy(Guy * g);

    Guy * attached_guy();

    FireCross * get_explosion();

    void set_explosion(FireCross * expl);

  private:
    int range;
    Guy * guy;
    FireCross * explosion;
};

#endif // BOMB_HPP
