/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SPRITES_MANAGER_HPP
#define SPRITES_MANAGER_HPP

#ifndef LIBNDS_HEADER_INCLUDED
#define LIBNDS_HEADER_INCLUDED
#   include <nds.h>
#endif // LIBNDS_HEADER_INCLUDED

#include <vector>

#include "graphics_utils.hpp"
#include "singleton.hpp"

class Sprites : public Singleton<Sprites> {

  public:

    // BACKGROUND

    enum BlockType
    {
        GRASS_BLOCK_ID,
        HARD_BRICK_BLOCK_ID,
        SOFT_BRICK_BLOCK_ID,

        FIRE_MIDDLE_BLOCK_ID,
        FIRE_DOWN_BLOCK_ID,
        FIRE_UP_BLOCK_ID,
        FIRE_LEFT_BLOCK_ID,
        FIRE_RIGHT_BLOCK_ID,
        FIRE_VERT_BLOCK_ID,
        FIRE_HORI_BLOCK_ID,
        FIRE_BLANK_BLOCK_ID

    };

	/* *
	 * This function allows to indiviadually set the blocks
	 * of the background(16x16)
	 * */
	void setBgBlock(int x, int y, BlockType blockType);
	void setFireBgBlock(int x, int y, BlockType blockType);

	void drawFireCross
	(
		s16 centerX, s16 centerY,
		s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
	);
	void clearFireCross
	(
		s16 centerX, s16 centerY,
		s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
	);

	// SETUP

	//void setupGraphics();

	void setup_graphics();
};



// SPRITES

enum
{

	SPRITE_GUY_DOWN_0 = 0,
	SPRITE_GUY_DOWN_1,
	SPRITE_GUY_DOWN_2,
	SPRITE_GUY_UP_0,
	SPRITE_GUY_UP_1,
	SPRITE_GUY_UP_2,
	SPRITE_GUY_LEFT_0,
	SPRITE_GUY_LEFT_1,
	SPRITE_GUY_LEFT_2,
	SPRITE_GUY_RIGHT_0,
	SPRITE_GUY_RIGHT_1,
	SPRITE_GUY_RIGHT_2,
	SPRITE_GUY_DIE_0,
	SPRITE_GUY_DIE_1,
	SPRITE_GUY_DIE_2,
	SPRITE_GUY_DIE_3,
	SPRITE_GUY_DIE_4,

	SPRITE_BOMB_0,
	SPRITE_BOMB_1,
	SPRITE_BOMB_2,

	SPRITE_ENEMY_0,
	SPRITE_ENEMY_1,
	SPRITE_ENEMY_2,
	SPRITE_ENEMY_3,

};

// palettes
enum
{
	PALETTE_GUY = 0
};

class ISprite {

  public:

    typedef void (*on_animation_end)(void*);

    struct animation_callback
    {
        on_animation_end func;
        void * object;
    };

	virtual ~ISprite();

	virtual void setPosition(s16 x, s16 y) = 0;
	virtual void setAnimation(int animation, struct animation_callback * callback = NULL) = 0;

	virtual void hide(bool b) = 0;

	// call this function in each frame
	virtual void update() = 0;

	int getSlot() const;

	virtual void setPriority(int priority) = 0;

  protected:

    ISprite();
	ISprite(int slot);

	virtual void setSprite(int sprite) = 0;
	virtual void setPalette(int palette) = 0;

	struct animation_callback anim_callback;

  private:
    int slot;
};


class Sprite : public ISprite {

  public:

	virtual ~Sprite() {};

	virtual void setPosition(s16 x, s16 y);
	virtual void setAnimation(int animation, struct animation_callback * callback = NULL);

	virtual void hide(bool b);

	// call this function in each frame
	virtual void update(){};

	virtual void setPriority(int priority);

  protected:

    Sprite() {};
	Sprite(int slot);
	SpriteAttrEntry * spriteEntry;

	virtual void setSprite(int sprite);
	virtual void setPalette(int palette);
};

class GuySprite : public Sprite
{

	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation

	void setSprite(int sprite);

public:

	GuySprite(int slot);
    ~GuySprite() {};

	enum
	{
		ANIMATION_STAND_DOWN,
		ANIMATION_STAND_UP,
		ANIMATION_STAND_LEFT,
		ANIMATION_STAND_RIGHT,
		ANIMATION_WALKING_DOWN,
		ANIMATION_WALKING_UP,
		ANIMATION_WALKING_LEFT,
		ANIMATION_WALKING_RIGHT,
		ANIMATION_DIE
	};

	void setAnimation(int animation, struct animation_callback * callback = NULL);

	void update();
};

class BombSprite : public Sprite
{

	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation

	void setSprite(int sprite);

public:

	BombSprite(int slot);
    ~BombSprite() {};

	enum
	{
		ANIMATION_DEFAULT,
		ANIMATION_EXPLODE
	};

	void setAnimation(int animation, struct animation_callback * callback = NULL);

	void update();

};

class EnemySprite : public Sprite
{

	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation

	void setSprite(int sprite);

public:

	EnemySprite(int slot);
    ~EnemySprite() {};

	enum
	{
		ANIMATION_DEFAULT,
		ANIMATION_DIE
	};

	void setAnimation(int animation, struct animation_callback * callback = NULL);

	void update();

};

class BlockSpriteFake : public ISprite {
  public:

	virtual ~BlockSpriteFake() {};

	virtual void setPosition(s16 x, s16 y);

	virtual void hide(bool b);

	// call this function in each frame
	virtual void update() {};

	virtual void setAnimation(int animation, struct animation_callback * callback = NULL);

	int getSlot() const;

  protected:

	BlockSpriteFake(int slot);

	virtual void setSprite(int block_type);

	virtual void setPalette(int palette);

	virtual void setPriority(int priority);

  private:
    int block_type;
    s16 bg_x;
    s16 bg_y;
};

class BreakableBlockSprite : public BlockSpriteFake
{

public:

	BreakableBlockSprite(int slot);
    ~BreakableBlockSprite() {};

	void setPosition(s16 x, s16 y);

	void hide(bool b);

	void setAnimation(int animation, struct animation_callback * callback = NULL);

	int getSlot() const;

  protected:
	void setSprite(int block_type);

	void setPalette(int palette);

	void setPriority(int priority);
};

class BreakableBlockSpriteAnim : public Sprite
{
	int animation;		// current animation
	int sprite;			// current sprite
	int frameCount;		// number of frames the current sprite is active
	int animationStep;	// current step of the animation

	void setSprite(int sprite);

public:

	BreakableBlockSpriteAnim(int slot);
    ~BreakableBlockSpriteAnim() {};

	enum
	{
		ANIMATION_DESTROY
	};

	void setAnimation(int animation, struct animation_callback * callback = NULL);

	void update();
};


class UnbreakableBlockSprite : public BlockSpriteFake
{

public:

	UnbreakableBlockSprite(int slot);
    ~UnbreakableBlockSprite() {};

	void setPosition(s16 x, s16 y);

	void hide(bool b);

	void setAnimation(int animation, struct animation_callback * callback = NULL);

	int getSlot() const;

  protected:
	void setSprite(int block_type);

	void setPalette(int palette);

	void setPriority(int priority);
};

/**
 * Use this singleton each time you want to
 * create a new sprite
 **/

class SpriteFactory : public Singleton<SpriteFactory>
{

private:

	std::vector<int> freeSlots;

public:

	SpriteFactory();

	GuySprite* createGuySprite();
	EnemySprite* createEnemySprite();
	BombSprite* createBombSprite();
	BreakableBlockSprite * createBreakableBlockSprite();
	BreakableBlockSpriteAnim * createBreakableBlockSpriteAnim();
	UnbreakableBlockSprite * createUnbreakableBlockSprite();

	void destroyGuySprite( GuySprite* guySprite );
	void destroyEnemySprite( EnemySprite* enemySprite );
	void destroyBombSprite( BombSprite* bombSprite );
	void destroyBreakableBlockSprite(BreakableBlockSprite * break_block);
    void destroyBreakableBlockAnimSprite(BreakableBlockSpriteAnim * break_block_anim);
    void destroyUnbreakableBlockSprite(UnbreakableBlockSprite * unbreak_block);
};

class FireCross {

  public:
	FireCross();

	FireCross
	(
		s16 centerX, s16 centerY,
		s16 downLimit, s16 upLimit, s16 leftLimit, s16 rightLimit
	);

	void draw();
	void clean();

  private:
	u16 centerX, centerY;
	u16 downLimit, upLimit, leftLimit, rightLimit;
};

#endif // SPRITES_MANAGER_HPP
