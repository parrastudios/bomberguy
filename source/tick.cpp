/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef LIBNDS_HEADER_INCLUDED
#define LIBNDS_HEADER_INCLUDED
#   include <nds.h>
#endif // LIBNDS_HEADER_INCLUDED

#include "tick.hpp"

void private_tick_callback() {
        TickManager::get_instance().tick();
}

TickManager::TickCallback::TickCallback(void * obj, on_tick_callback c, int f, bool l) :
    object(obj), callback(c), freq(f), count(0), loop(l) {

}

TickManager::TickCallback::~TickCallback() {

}

bool TickManager::TickCallback::tick() {
    ++count;

	if (count >= freq) {
		count = 0;

		if (callback) {
            callback(object);
		}

        return true;
    }

    return false;
}

bool TickManager::TickCallback::is_loop() {
    return loop;
}

void TickManager::initialize(int frequency) {

        clear();

        // define tick as a N period of times per second
        timerStart(0, ClockDivider_1024, TIMER_FREQ_1024(frequency), private_tick_callback);
}

void TickManager::clear() {
	callback_list.clear();
	to_insert_list.clear();
	inserting = false;
}

void TickManager::tick() {
    std::vector<TickCallback>::iterator it = callback_list.begin();

    while (it != callback_list.end()) {
        TickCallback & cb = *it;

        if (cb.tick()) {
            if (!cb.is_loop()) {
                it = callback_list.erase(it);
            } else {
                ++it;
            }
        } else {
            ++it;
        }
    }

    inserting = true;

    while (!to_insert_list.empty()) {
        callback_list.push_back(to_insert_list.back());
        to_insert_list.pop_back();
    }

    inserting = false;
}

void TickManager::register_callback(void * object, on_tick_callback callback, int tick_freq, bool loop) {
    while (inserting);

    to_insert_list.push_back(TickCallback(object, callback, tick_freq, loop));
}
