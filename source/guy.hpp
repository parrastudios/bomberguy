/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef GUY_HPP
#define GUY_HPP

#include "entity.hpp"

class Bomb;

class Guy : public Entity {

  public:

	static const int GUY_TYPE_ID_KEY = 0x01;

	Guy(Scene * s);
    Guy(Scene * s, int initial_x, int initial_y);
    virtual ~Guy();

	virtual void update();
	virtual void render();

	virtual void on_collide_callback(Entity * collider);

	virtual void on_bomb_callback();

	void create_bomb();

	bool is_bomb_thrown();

	void set_bomb_thrown(bool t);

	bool is_above_bomb();

	ISprite::on_animation_end get_death_callback();

  protected:
    virtual void begin_movement(int movement);

    virtual void end_movement(int movement);

  private:
	bool is_on_bomb;
	Bomb * bomb;
	bool bomb_thrown;
};

#endif // GUY_HPP
