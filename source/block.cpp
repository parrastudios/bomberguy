/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "block.hpp"
#include "behavior.hpp"
#include "scene.hpp"

Block::Block(int id, Scene * s) : Entity(id, s) {
	set_behavior(new NullBehavior(*this));
	set_collidable(true);
}

Block::Block(int id, Scene * s, int initial_x, int initial_y) : Entity(id, s, initial_x, initial_y) {
	set_behavior(new NullBehavior(*this));
	set_collidable(true);
}

Block::~Block() {

}

void Block::update() {


	Entity::update();
}

void Block::render() {

	Entity::render();
}

void Block::on_collide_callback(Entity * collider) {

    Entity::on_collide_callback(collider);
}

void Block::on_bomb_callback() {

    Entity::on_bomb_callback();
}

BreakableBlock::BreakableBlock(Scene * s) : Block(BREAKABLE_BLOCK_TYPE_ID_KEY, s) {

	set_sprite(SpriteFactory::get_instance().createBreakableBlockSprite());
	set_collidable(true);
}

BreakableBlock::BreakableBlock(Scene * s, int initial_x, int initial_y) : Block(BREAKABLE_BLOCK_TYPE_ID_KEY, s, initial_x, initial_y) {

	set_sprite(SpriteFactory::get_instance().createBreakableBlockSprite());
	get_sprite()->setPosition((s16)initial_x, (s16)initial_y);
	set_collidable(true);
}

BreakableBlock::~BreakableBlock() {
    SpriteFactory::get_instance().destroyBreakableBlockSprite((BreakableBlockSprite *)sprite);
}

void BreakableBlock::update() {

	Block::update();
}

void BreakableBlock::render() {

	Block::render();
}

void BreakableBlock::on_collide_callback(Entity * collider) {

    Block::on_collide_callback(collider);
}

void BreakableBlock::on_bomb_callback() {

	// give a powerup ?

    Block::on_bomb_callback();
}

UnbreakableBlock::UnbreakableBlock(Scene * s) : Block(UNBREAKABLE_BLOCK_TYPE_ID_KEY, s) {

	set_sprite(SpriteFactory::get_instance().createUnbreakableBlockSprite());
	set_collidable(true);
}

UnbreakableBlock::UnbreakableBlock(Scene * s, int initial_x, int initial_y) : Block(UNBREAKABLE_BLOCK_TYPE_ID_KEY, s, initial_x, initial_y) {

    set_sprite(SpriteFactory::get_instance().createUnbreakableBlockSprite());
	set_collidable(true);
}

UnbreakableBlock::~UnbreakableBlock() {
    SpriteFactory::get_instance().destroyUnbreakableBlockSprite((UnbreakableBlockSprite *)sprite);
}

void UnbreakableBlock::update() {

	Block::update();
}

void UnbreakableBlock::render() {

	Block::render();
}

void UnbreakableBlock::on_collide_callback(Entity * collider) {


    Block::on_collide_callback(collider);
}

void UnbreakableBlock::on_bomb_callback() {


    Block::on_bomb_callback();
}
