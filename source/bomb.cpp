/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "bomb.hpp"
#include "behavior.hpp"
#include "scene.hpp"
#include "sprites.hpp"
#include "tick.hpp"
#include "entity_manager.hpp"
#include "guy.hpp"

#define BOMB_EXPLODE_PERIOD         150
#define BOMB_EXPLODE_RANGE_DEFAULT  1

void private_bomb_explode_end_callback(void * object) {
    Bomb * bomb = (Bomb *)object;

    bomb->set_valid(false);

    bomb->attached_guy()->set_bomb_thrown(false);

    bomb->get_explosion()->clean();
}

void private_bomb_explode_callback(void * object) {
    Bomb * bomb = (Bomb *)object;

    // explode the bomb
    bomb->explode();

    struct Sprite::animation_callback cb = {
        &private_bomb_explode_end_callback,
        bomb
    };

    bomb->get_sprite()->setAnimation(BombSprite::ANIMATION_EXPLODE, &cb);
}

Bomb::Bomb(Scene * s) : Entity(BOMB_TYPE_ID_KEY, s), range(BOMB_EXPLODE_RANGE_DEFAULT), guy(NULL) {
    set_sprite(SpriteFactory::get_instance().createBombSprite());
    sprite->setPriority(1);
	set_behavior(new NullBehavior(*this));
	set_collidable(false);
	set_valid(false);
}

Bomb::Bomb(Scene * s, int initial_x, int initial_y) : Entity(BOMB_TYPE_ID_KEY, s, initial_x, initial_y), range(BOMB_EXPLODE_RANGE_DEFAULT), guy(NULL) {
    set_sprite(SpriteFactory::get_instance().createBombSprite());
    sprite->setPriority(1);
	set_behavior(new NullBehavior(*this));
	set_collidable(false);
	set_valid(false);
}

Bomb::~Bomb() {
    SpriteFactory::get_instance().destroyBombSprite((BombSprite *)sprite);
}

void Bomb::update() {


	Entity::update();
}

void Bomb::render() {

	Entity::render();
}

void Bomb::on_collide_callback(Entity * collider) {

    Entity::on_collide_callback(collider);
}

void Bomb::on_bomb_callback() {

    Entity::on_bomb_callback();
}

void Bomb::countdown() {

    sprite->setAnimation(BombSprite::ANIMATION_DEFAULT, NULL);

	TickManager::get_instance().register_callback(this, &private_bomb_explode_callback, BOMB_EXPLODE_PERIOD, false);
}

void Bomb::explode() {
    // apply logic
    scene->entity_remove_explosion(this);
}

int Bomb::get_range() {
    return range;
}

void Bomb::set_range(int r) {
    range = r;
}

void Bomb::attach_to_guy(Guy * g) {
    guy = g;
}

Guy * Bomb::attached_guy() {
    return guy;
}

FireCross * Bomb::get_explosion() {
    return explosion;
}

void Bomb::set_explosion(FireCross * expl) {
    if (explosion) {
        delete explosion;
    }

    explosion = expl;
}
