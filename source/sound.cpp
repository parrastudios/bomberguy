/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "sound.hpp"

#include "soundbank.h"
#include "soundbank_bin.h"

//extern mm_sound_effect collideSound;

void SoundManager::setup_sound() {

    // initialize engine
	soundEnable();
	mmInitDefaultMem((mm_addr)soundbank_bin);

    // initialize themes
	mmLoad(MOD_POKEMON_GYM);

	// initialize effects
	mmLoadEffect(SFX_COLLIDE);

	// setup effects
	sound_initialize(SOUND_COLLISION, SFX_COLLIDE, (int)(1.0f * (1<<10)), 0, 255, 127);
}

void SoundManager::set_main_theme(SoundTheme theme) {

	if (theme == THEME_POKEMON) {
		mmStart(MOD_POKEMON_GYM, MM_PLAY_LOOP);
		mmSetModuleVolume(255);
	}
}

void SoundManager::play(SoundTrack track) {

	mmEffectEx(&sound_list[track]);
}

void SoundManager::sound_initialize(SoundTrack track, mm_word id, mm_hword rate, mm_sfxhand handle, mm_byte volume, mm_byte panning) {
    sound_list[track].id = id;
    sound_list[track].rate = rate;
    sound_list[track].handle = handle;
    sound_list[track].volume = volume;
    sound_list[track].panning = panning;
}
