/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SOUND_HPP
#define SOUND_HPP

#ifndef LIBNDS_HEADER_INCLUDED
#define LIBNDS_HEADER_INCLUDED
#   include <nds.h>
#endif // LIBNDS_HEADER_INCLUDED

#ifndef LIBMAXMOD_HEADER_INCLUDED
#define LIBMAXMOD_HEADER_INCLUDED
#   include <maxmod9.h>
#endif // LIBMAXMOD_HEADER_INCLUDED

#include "singleton.hpp"

class SoundManager : public Singleton<SoundManager> {
  public:

    enum SoundTheme
    {
        THEME_POKEMON = 0x00,

        THEME_LIST_SIZE
    };

    enum SoundTrack
    {
        SOUND_COLLISION = 0x00,
        SOUND_DIE = 0x01,

        SOUND_LIST_SIZE
    };

    void setup_sound();

    void set_main_theme(SoundTheme theme);

    void play(SoundTrack track);

  private:

    void sound_initialize(SoundTrack track, mm_word id, mm_hword rate, mm_sfxhand handle, mm_byte volume, mm_byte panning);

    // .. (theme list)

    // sound list
    mm_sound_effect sound_list[SOUND_LIST_SIZE];
};

#endif
