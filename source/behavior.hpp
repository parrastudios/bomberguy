/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef BEHAVIOR_HPP
#define BEHAVIOR_HPP

#include "entity.hpp"

class Behavior {

  public:
	Behavior(Entity & ent);
	virtual ~Behavior();

	virtual void execute() = 0;

  protected:
	friend class Entity;

	Entity & entity;
};

class NullBehavior : public Behavior {
  public:
	NullBehavior(Entity & ent);
	~NullBehavior();

	virtual void execute();
};

class RandomBehavior : public Behavior {
  public:
	RandomBehavior(Entity & ent);
	~RandomBehavior();

	virtual void execute();

  private:
	int prev_movement;
	static int randomize_movement();
};

class FollowBehavior : public Behavior {
  public:
	FollowBehavior(Entity & ent, Entity & tar);
	~FollowBehavior();

	virtual void execute();

	void set_target(Entity & t);

  private:
	Entity& target;
};

class UserInputBehavior : public Behavior {
  public:
	UserInputBehavior(Entity & ent);
	~UserInputBehavior();

	virtual void execute();
};

#endif // BEHAVIOR_HPP
