/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "entity.hpp"

class Block : public Entity {

  public:

	static const int BLOCK_TYPE_ID_KEY = 0x03;

    virtual ~Block();

	virtual void update();
	virtual void render();

    virtual void on_collide_callback(Entity * collider);

    virtual void on_bomb_callback();

  protected:

	Block(int id, Scene * s);
    Block(int id, Scene * s, int initial_x, int initial_y);
};

class BreakableBlock : public Block {

  public:

	static const int BREAKABLE_BLOCK_TYPE_ID_KEY = 0x04;

	BreakableBlock(Scene * s);
    BreakableBlock(Scene * s, int initial_x, int initial_y);
    virtual ~BreakableBlock();

	virtual void update();
	virtual void render();

	virtual void on_collide_callback(Entity * collider);

	virtual void on_bomb_callback();
};

class UnbreakableBlock : public Block {

  public:

    static const int UNBREAKABLE_BLOCK_TYPE_ID_KEY = 0x05;

	UnbreakableBlock(Scene * s);
	UnbreakableBlock(Scene * s, int initial_x, int initial_y);
	virtual ~UnbreakableBlock();

	virtual void update();
	virtual void render();

    virtual void on_collide_callback(Entity * collider);

    virtual void on_bomb_callback();
};

#endif // BLOCK_HPP
