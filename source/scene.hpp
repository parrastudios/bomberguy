/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef SCENE_HPP
#define SCENE_HPP

#include "entity.hpp"
#include "bomb.hpp"

// define map size
#define MAP_TILE_SIZE_WIDTH     16
#define MAP_TILE_SIZE_HEIGHT    12

#define TILE_SIZE_WIDTH	    	16
#define TILE_SIZE_HEIGHT	    16

#define MAP_TILE_COUNT	    	MAP_TILE_SIZE_WIDTH * MAP_TILE_SIZE_HEIGHT

class Node {
  public:

        Node();
        Node(Entity * e);
        ~Node();

        Entity * entity;
        Node * next;
};

class Scene {

  public:
    enum {
        SCENE_TEST_ID = 0x00,
        SCENE_RANDOM_ID = 0x01
    };

	Scene();

	~Scene();

	void load(int index);

	void clear();

	bool entity_create(Entity * entity);

	bool entity_move(Entity * entity, int new_x, int new_y);

	bool entity_clamp_velocity(Entity * entity, int vel_x, int vel_y);

	bool entity_remove(Entity * entity);

	void entity_remove_explosion_by_index(int index);

	void entity_remove_explosion(Bomb * bomb);

  private:
	bool entity_create_by_index(Entity * entity, int index);

	bool entity_remove_by_index(Entity * entity, int index);

	void entity_can_reach_position(Entity * entity, int pos_x, int pos_y);

	Node entity_data[MAP_TILE_COUNT];
};

#endif // SCENE_HPP
