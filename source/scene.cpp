/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "scene.hpp"
#include "clamp.hpp"
#include "entity_manager.hpp"
#include "randomizer.hpp"

#define MAP_INDEX(x, y) \
	(y) * MAP_TILE_SIZE_WIDTH + (x)

#define SCENE_DEFAULT_ENEMY_SIZE 5
#define SCENE_DEFAULT_BREAK_BLOCK_SIZE 45

Node::Node() : entity(NULL), next(NULL) {

}

Node::Node(Entity * e) : entity(e), next(NULL) {

}

Node::~Node() {

}

Scene::Scene() {
	clear();
}

Scene::~Scene() {

}

void Scene::load(int index) {
	clear();

    // blocks are always the same, so create it statically
    for (int x = 0; x < MAP_TILE_SIZE_WIDTH; x += 2) {
        for (int y = 0; y < MAP_TILE_SIZE_HEIGHT; y += 2) {
            // insert a unbreakable block into the scene
            EntityManager::get_instance().create_unbreakable_block(this, x, y);
        }
    }

    if (index == SCENE_TEST_ID) {
        // insert player into the scene
        EntityManager::get_instance().create_guy(this, 0, 1);

        // insert a enemy into the scene
        EntityManager::get_instance().create_stupid_enemy(this, 4, 5);

        // insert a breakable block into the scene
        EntityManager::get_instance().create_breakable_block(this, 3, 2);
    } else if (index == SCENE_RANDOM_ID) {

        int entity_count = 0;

        // insert random enemies
        while (entity_count < SCENE_DEFAULT_ENEMY_SIZE) {

            int rand_x = Randomizer::get_instance().random(0, MAP_TILE_SIZE_WIDTH - 1);
            int rand_y = Randomizer::get_instance().random(0, MAP_TILE_SIZE_HEIGHT - 1);
            int rand_index = MAP_INDEX(rand_x, rand_y);

            Node * it = &entity_data[rand_index];

            if (it->entity == NULL || (it->entity != NULL && !it->entity->is_collidable())) {
                // insert a enemy into the scene
                EntityManager::get_instance().create_stupid_enemy(this, rand_x, rand_y);
                ++entity_count;
            }
        }

        entity_count = 0;

        // insert random breakable blocks
        while (entity_count < SCENE_DEFAULT_BREAK_BLOCK_SIZE) {

            int rand_x = Randomizer::get_instance().random(0, MAP_TILE_SIZE_WIDTH - 1);
            int rand_y = Randomizer::get_instance().random(0, MAP_TILE_SIZE_HEIGHT - 1);
            int rand_index = MAP_INDEX(rand_x, rand_y);

            Node * it = &entity_data[rand_index];

            if (it->entity == NULL) {
                // insert a enemy into the scene
                EntityManager::get_instance().create_breakable_block(this, rand_x, rand_y);
                ++entity_count;
            }
        }

        entity_count = 0;

        // insert random guy
        while (!entity_count) {
            int rand_x = Randomizer::get_instance().random(0, MAP_TILE_SIZE_WIDTH - 1);
            int rand_y = Randomizer::get_instance().random(0, MAP_TILE_SIZE_HEIGHT - 1);
            int rand_index = MAP_INDEX(rand_x, rand_y);

            Node * it = &entity_data[rand_index];

            if (it->entity == NULL) {
                // insert a enemy into the scene
                EntityManager::get_instance().create_guy(this, rand_x, rand_y);
                entity_count = 1;
            }
        }
    }
}

void Scene::clear() {
	for (int i = 0; i < MAP_TILE_COUNT; ++i) {

		Node * it = entity_data[i].next;

		entity_data[i].entity = NULL;

		while (it) {
			Node * next = it->next;

			delete it;

			it = next;
		}
	}
}

bool Scene::entity_create_by_index(Entity * entity, int index) {

	Node * it = &entity_data[index];

	while (it) {
        if (it->entity == entity) {
            return true;
        }

		if (it->entity == NULL) {
			it->entity = entity;
			return true;
		} else {
			if (it->next == NULL) {
				it->next = new Node(entity);
				return true;
			}
		}

		it = it->next;
	}

	return false;
}

bool Scene::entity_create(Entity * entity) {

	int index = MAP_INDEX(entity->get_x(), entity->get_y());

	return entity_create_by_index(entity, index);
}

bool Scene::entity_move(Entity * entity, int new_x, int new_y) {

    bool collision = false;

	// calculate current position
	int cur_index = MAP_INDEX(entity->get_x(), entity->get_y());

	// clamp new position to the screen
	int clamped_x, clamped_y;
	int clamped_index;

	if (ClampInt::value(clamped_x, new_x, 0, MAP_TILE_SIZE_WIDTH - 1) ||
		ClampInt::value(clamped_y, new_y, 0, MAP_TILE_SIZE_HEIGHT - 1)) {

		entity->set_movement(Entity::MOVE_NONE);
		return false;
	}

	clamped_index = MAP_INDEX(clamped_x, clamped_y);

	if (cur_index != clamped_index) {

		if (entity_data[clamped_index].entity != NULL) {

			Node * it = &entity_data[clamped_index];

			while (it && it->entity) {

				// send event to the collided entity
				if (it->entity != entity_data[cur_index].entity) {
                    it->entity->on_collide_callback(entity_data[cur_index].entity);
				}

				// collision flag
				collision = (collision || it->entity->is_collidable());

				it = it->next;
			}
		}
	}

    if (!collision) {
        // move the entity in the table
		if (entity_create_by_index(entity, clamped_index)) {
			entity_remove_by_index(entity, cur_index);
		}
    }

    return !collision;
}

bool Scene::entity_remove_by_index(Entity * entity, int index) {

	Node * it = &entity_data[index];
	Node * prev = NULL;

	while (it && it->entity && it->entity != entity) {
		prev = it;
		it = it->next;
	}

	if (it != NULL) {
		if (prev == NULL) {
			if (it->entity && it->entity == entity) {

				if (it->next == NULL) {
					it->entity = NULL;
				} else {
					Node * next = it->next;

					it->entity = next->entity;
					it->next = next->next;

					delete next;
				}

				return true;
			} else {
				return false;
			}
		} else {
			prev->next = it->next;
			delete it;
			return true;
		}
	}

	return false;
}

bool Scene::entity_remove(Entity * entity) {

	int index = MAP_INDEX(entity->get_x(), entity->get_y());

	return entity_remove_by_index(entity, index);
}

void Scene::entity_remove_explosion_by_index(int index) {
    Node * it = &entity_data[index];

    if (it->entity == NULL) {
        return;
    } else {

        it->entity->on_bomb_callback();

        it->entity->set_valid(false);

        it->entity = NULL;

        it = it->next;

        while (it) {

            if (it->entity != NULL) {
                Node * next = it->next;

                it->entity->on_bomb_callback();

                it->entity->set_valid(false);

                delete it;

                it = next;

            } else {
                delete it;
                return;
            }
        }
    }
}

void Scene::entity_remove_explosion(Bomb * bomb) {
    // do the logic
    int right, left, up, down;

    // go to right
    int tile_count = 1;

    while ((bomb->get_x() + tile_count) < MAP_TILE_SIZE_WIDTH &&
           entity_data[MAP_INDEX(bomb->get_x() + tile_count, bomb->get_y())].entity == NULL &&
           tile_count <= bomb->get_range()) {
        ++tile_count;
    }

    if (tile_count <= bomb->get_range()) {
        int index = MAP_INDEX(bomb->get_x() + tile_count, bomb->get_y());

        if (entity_data[index].entity != NULL &&
            entity_data[index].entity->get_type_id() != UnbreakableBlock::UNBREAKABLE_BLOCK_TYPE_ID_KEY) {
                entity_remove_explosion_by_index(index);
                right = tile_count;
        } else {
            right = tile_count - 1;
        }
    } else {
        right = tile_count - 1;
    }

    // go to left
    tile_count = 1;

    while ((bomb->get_x() - tile_count) >= 0 &&
           entity_data[MAP_INDEX(bomb->get_x() - tile_count, bomb->get_y())].entity == NULL &&
           tile_count <= bomb->get_range()) {
        ++tile_count;
    }

    if (tile_count <= bomb->get_range()) {
        int index = MAP_INDEX(bomb->get_x() - tile_count, bomb->get_y());

        if (entity_data[index].entity != NULL &&
            entity_data[index].entity->get_type_id() != UnbreakableBlock::UNBREAKABLE_BLOCK_TYPE_ID_KEY) {
                entity_remove_explosion_by_index(index);
                left = tile_count;
        } else {
            left = tile_count - 1;
        }
    } else {
        left = tile_count - 1;
    }

    // go to up
    tile_count = 1;

    while ((bomb->get_y() - tile_count) >= 0 &&
           entity_data[MAP_INDEX(bomb->get_x(), bomb->get_y() - tile_count)].entity == NULL &&
           tile_count <= bomb->get_range()) {
        ++tile_count;
    }

    if (tile_count <= bomb->get_range()) {
        int index = MAP_INDEX(bomb->get_x(), bomb->get_y() - tile_count);

        if (entity_data[index].entity != NULL &&
            entity_data[index].entity->get_type_id() != UnbreakableBlock::UNBREAKABLE_BLOCK_TYPE_ID_KEY) {
                entity_remove_explosion_by_index(index);
                up = tile_count;
        } else {
            up = tile_count - 1;
        }
    } else {
        up = tile_count - 1;
    }

    // go to down
    tile_count = 1;

    while ((bomb->get_y() + tile_count) < MAP_TILE_SIZE_HEIGHT &&
           entity_data[MAP_INDEX(bomb->get_x(), bomb->get_y() + tile_count)].entity == NULL &&
           tile_count <= bomb->get_range()) {
        ++tile_count;
    }

    if (tile_count <= bomb->get_range()) {
        int index = MAP_INDEX(bomb->get_x(), bomb->get_y() + tile_count);

        if (entity_data[index].entity != NULL &&
            entity_data[index].entity->get_type_id() != UnbreakableBlock::UNBREAKABLE_BLOCK_TYPE_ID_KEY) {
                entity_remove_explosion_by_index(index);

                down = tile_count;
        } else {
            down = tile_count - 1;
        }
    } else {
        down = tile_count - 1;
    }

    // check if the guy is above the bomb
    if (bomb->attached_guy()->is_above_bomb()) {

        bomb->attached_guy()->on_bomb_callback();
        bomb->attached_guy()->set_valid(false);
    }

    // clamp explosion range
    ClampInt::value(left, bomb->get_x() - left, 0, MAP_TILE_SIZE_WIDTH - 1);
    ClampInt::value(right, bomb->get_x() + right, 0, MAP_TILE_SIZE_WIDTH - 1);

    ClampInt::value(up, bomb->get_y() - up, 0, MAP_TILE_SIZE_HEIGHT - 1);
    ClampInt::value(down, bomb->get_y() + down, 0, MAP_TILE_SIZE_HEIGHT - 1);

    // create explosion
    bomb->set_explosion(new FireCross(bomb->get_x(), bomb->get_y(), down, up, left, right));

    // render explosion
    bomb->get_explosion()->draw();
}
