/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "entity.hpp"
#include "behavior.hpp"

class Enemy : public Entity {

  public:

	static const int ENEMY_TYPE_ID_KEY = 0x02;

	Enemy(Scene * s);
    Enemy(Scene * s, int initial_x, int initial_y);
    virtual ~Enemy();

	virtual void update();
	virtual void render();

    virtual void on_collide_callback(Entity * collider);

    virtual void on_bomb_callback();

};

class StupidEnemy : public Enemy {
  public:
    StupidEnemy(Scene * s);
    StupidEnemy(Scene * s, int initial_x, int initial_y);
    virtual ~StupidEnemy();

    virtual void on_collide_callback(Entity * collider);

    virtual void on_bomb_callback();
};

class CleverEnemy : public Enemy {
  public:
	CleverEnemy(Scene * s, Entity& target);
	CleverEnemy(Scene * s, int initial_x, int initial_y, Entity & target);
	virtual ~CleverEnemy();

	void set_target(Entity& target);

    virtual void on_collide_callback(Entity * collider);

    virtual void on_bomb_callback();
};

#endif // ENEMY_HPP
