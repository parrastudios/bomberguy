/////////////////////////////////////////////////////////////////////////////
//  Bomberguy
//
//  A Bomberman remake developed in C++ for NintendoDS using devkitPro.
//
//  Copyright (C) 2015
//	Vicente Eduardo Ferrer García - vic798@gmail.com
//	Alejandro Juan Pérez - tuketelamodelmon@gmail.com
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/> or
//  <http://www.affero.org/oagpl.html>.
/////////////////////////////////////////////////////////////////////////////

#include "guy.hpp"
#include "behavior.hpp"
#include "scene.hpp"
#include "enemy.hpp"
#include "sprites.hpp"
#include "entity_manager.hpp"
#include "bomb.hpp"

void private_on_guy_death(void * object) {
    Guy * guy = (Guy *)object;

    guy->set_valid(false);
}

Guy::Guy(Scene * s) : Entity(GUY_TYPE_ID_KEY, s), is_on_bomb(false),
                        bomb(EntityManager::get_instance().create_bomb(get_scene(), get_x(), get_y())), bomb_thrown(false) {

	set_behavior(new UserInputBehavior(*this));
	set_sprite(SpriteFactory::get_instance().createGuySprite());
	sprite->setAnimation(GuySprite::ANIMATION_STAND_DOWN);
	bomb->set_valid(false);
	bomb->attach_to_guy(this);
}

Guy::Guy(Scene * s, int initial_x, int initial_y) : Entity(GUY_TYPE_ID_KEY, s, initial_x, initial_y), is_on_bomb(false),
                                                    bomb(EntityManager::get_instance().create_bomb(get_scene(), get_x(), get_y())),
                                                    bomb_thrown(false) {

	set_behavior(new UserInputBehavior(*this));
	set_sprite(SpriteFactory::get_instance().createGuySprite());
	sprite->setAnimation(GuySprite::ANIMATION_STAND_DOWN);
	bomb->set_valid(false);
	bomb->attach_to_guy(this);
}

Guy::~Guy() {
    SpriteFactory::get_instance().destroyGuySprite((GuySprite *)sprite);
}

void Guy::update() {

    if (bomb) {
        if (bomb->get_x() != get_x() || bomb->get_y() != get_y()) {
            is_on_bomb = false;
            bomb->set_collidable(true);
            set_collidable(true);
        }
    } else {
        is_on_bomb = false;
    }

	Entity::update();
}

void Guy::render() {

	// ..

	Entity::render();
}

void Guy::on_collide_callback(Entity * collider) {

    if (!is_on_bomb) {
        if (collider->get_type_id() == Enemy::ENEMY_TYPE_ID_KEY) {

            struct Sprite::animation_callback cb = {
                &private_on_guy_death,
                this
            };

            being_invalidated();

            // remove from scene
            scene->entity_remove(this);

            // execute animation
            sprite->setAnimation(GuySprite::ANIMATION_DIE, &cb);
        }
    }

    Entity::on_collide_callback(collider);
}

void Guy::on_bomb_callback() {
    struct Sprite::animation_callback cb = {
        &private_on_guy_death,
        this
    };

    being_invalidated();

    // remove from scene
    scene->entity_remove(this);

    // execute animation
    sprite->setAnimation(GuySprite::ANIMATION_DIE, &cb);

	// restart game ?
	// ..

    Entity::on_bomb_callback();
}

void Guy::create_bomb() {
    if (!is_bomb_thrown()) {
        set_collidable(false);

        is_on_bomb = true;

        if (scene->entity_remove(bomb)) {
            bomb->set_x(get_x()); bomb->set_y(get_y());
            scene->entity_create(bomb);
        }

        bomb->set_valid(true);

        bomb->set_collidable(false);

        set_bomb_thrown(true);

        bomb->countdown();
    }
}

void Guy::begin_movement(int movement) {

    if (movement == Entity::MOVE_DOWN) {
        sprite->setAnimation(GuySprite::ANIMATION_WALKING_DOWN);
    } else if (movement == Entity::MOVE_UP) {
        sprite->setAnimation(GuySprite::ANIMATION_WALKING_UP);
    } else if (movement == Entity::MOVE_LEFT) {
        sprite->setAnimation(GuySprite::ANIMATION_WALKING_LEFT);
    } else if (movement == Entity::MOVE_RIGHT) {
        sprite->setAnimation(GuySprite::ANIMATION_WALKING_RIGHT);
    }

    Entity::begin_movement(movement);
}

void Guy::end_movement(int movement) {

    if (movement == Entity::MOVE_DOWN) {
        sprite->setAnimation(GuySprite::ANIMATION_STAND_DOWN);
    } else if (movement == Entity::MOVE_UP) {
        sprite->setAnimation(GuySprite::ANIMATION_STAND_UP);
    } else if (movement == Entity::MOVE_LEFT) {
        sprite->setAnimation(GuySprite::ANIMATION_STAND_LEFT);
    } else if (movement == Entity::MOVE_RIGHT) {
        sprite->setAnimation(GuySprite::ANIMATION_STAND_RIGHT);
    }

    Entity::end_movement(movement);
}

bool Guy::is_bomb_thrown() {
    return bomb_thrown;
}

void Guy::set_bomb_thrown(bool t) {
    bomb_thrown = t;
}

bool Guy::is_above_bomb() {
    return is_on_bomb;
}

ISprite::on_animation_end Guy::get_death_callback() {
    return &private_on_guy_death;
}
